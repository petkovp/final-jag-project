-- Best practice MySQL
--
-- FOREIGN KEYS NAMES ARE NAMED AFTER THE FOLLOWING CONVENTION :
-- fk_[referencing table name]_[referenced table name]_[referencing field name]
USE MAILAPP;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS FOLDER;
DROP TABLE IF EXISTS EMAIL;
DROP TABLE IF EXISTS ADDRESSES;
DROP TABLE IF EXISTS EMAILBRADDRESSES;
DROP TABLE IF EXISTS BINARYFILES;
DROP TABLE IF EXISTS EMAILBRBINARYFILES;
SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE FOLDER (
  ID INT NOT NULL auto_increment,
  FOLDERNAME varchar(45) NOT NULL default '',
  PRIMARY KEY  (ID)
);

CREATE TABLE EMAIL (
  ID INT NOT NULL auto_increment,
  FOLDERKEY INT NOT NULL default 0,
  DATE DATETIME, 
  SUBJECT TEXT,
  MESSAGE LONGTEXT,
  HTML LONGTEXT,
  FROMWHO varchar(320), 


PRIMARY KEY  (ID)
);

 

CREATE TABLE ADDRESSES (
  ID INT NOT NULL auto_increment,
  ADDRESS varchar(320), 
   


PRIMARY KEY  (ID)
);
--TYPE INDICATES IF THE ADDRESS IS A  BCC, CC OR TO
CREATE TABLE EMAILBRADDRESSES(
TYPE VARCHAR(4) NOT NULL,
EMAILID INT NOT NULL,
ADDRESSID INT NOT NULL,
CONSTRAINT FK_EMAILBRADDRESSES_EMAIL_EMAILID FOREIGN KEY (EMAILID) REFERENCES EMAIL(ID),
CONSTRAINT FK_EMAILBRADDRESSES_ADDRESS_ADDRESSID FOREIGN KEY (ADDRESSID) REFERENCES ADDRESSES(ID)
);

CREATE TABLE BINARYFILES (
  ID int NOT NULL auto_increment PRIMARY KEY,
  FILENAME varchar(128) NOT NULL default '',
  BINARYDATA mediumblob
) ENGINE=InnoDB;

-- TYPE INDICATES IF IT IS A REGULAR ATTACHMENT OR AN EMBEDDED ONE
CREATE TABLE EMAILBRBINARYFILES(
TYPE VARCHAR(8) NOT NULL,
EMAILID INT NOT NULL,
FILEID INT NOT NULL,
CONSTRAINT FK_EMAILBRBINARYFILES_EMAIL_EMAILID FOREIGN KEY (EMAILID) REFERENCES EMAIL(ID),
CONSTRAINT FK_EMAILBRBINARYFILES_BINARYFILES_FILEID FOREIGN KEY (FILEID) REFERENCES BINARYFILES(ID)
);



INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (1, '1983-02-06 12:26:07',0, 'Non reiciendis iure voluptas minus iure et non quisquam.', 'Modi molestias voluptatem nemo autem quia repudiandae. Voluptas laborum commodi expedita.', NULL, 'hoppe.terry@example.org');
INSERT INTO `EMAIL` (`ID`, `DATE`, FOLDERKEY,`SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (2, '2000-06-30 19:09:44',0, 'Repellendus error consectetur perferendis aut.', 'Officiis praesentium enim repellat aut et rerum et. Est necessitatibus autem et occaecati recusandae beatae voluptates in. Eius sed in perferendis sed nostrum explicabo perferendis. Quis molestiae nam sed vel quasi dolores.', NULL, 'ward.yessenia@example.org');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (3, '2005-10-11 12:10:19',0, 'Iste odio sint cumque nobis officiis.', 'Odit fugiat debitis ipsam reiciendis. Et aut aut corrupti cum voluptatem.', NULL, 'tabitha19@example.com');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (4, '2001-06-27 04:43:02',0, 'Ut corporis id voluptatem incidunt ducimus.', 'Rerum molestias aut fugit ab dolor rerum ipsa. Facere ex atque doloremque a debitis. Molestiae doloremque dolores est dolorem.', NULL, 'rwolf@example.org');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (5, '1984-04-16 13:07:49',1, 'Libero voluptates est maxime inventore nisi perspiciatis molestiae ipsa.', 'Quo rem dolores ea voluptates. Dolor sapiente incidunt numquam facilis. Numquam aperiam libero harum unde. Laborum a et necessitatibus voluptatem est unde.', NULL, 'myrtis16@example.com');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (6, '2003-02-16 05:36:16',1, 'Voluptatem dolore dolorem animi et facere veritatis.', 'Ullam est magni qui quia similique excepturi quia. Voluptatem dignissimos sed omnis. Dolorum excepturi ut vitae occaecati id provident ipsa.', NULL, 'antonette.beatty@example.net');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (7, '1970-07-16 00:16:43',1, 'Aut temporibus fugit voluptate qui consequatur reiciendis nesciunt.', 'Quis saepe natus quia ullam in ducimus distinctio officiis. Enim eligendi omnis qui mollitia sunt recusandae earum dicta. Veritatis et eius minus non qui soluta consectetur. Voluptas omnis inventore consectetur in quia nihil.', NULL, 'wilhelmine.abernathy@example.com');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (8, '2003-07-15 07:22:36',1, 'Enim voluptatem placeat nihil quas consequatur atque repudiandae ullam.', 'Rem quibusdam et et. Odio aperiam porro officiis aut. In explicabo ipsam quia neque voluptatem beatae. Aut dignissimos nesciunt non quas earum.', NULL, 'lking@example.net');
INSERT INTO `EMAIL` (`ID`, `DATE`, FOLDERKEY,`SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (9, '1995-03-12 17:48:24',1, 'Et odit voluptatem dolores molestias officiis ipsum.', 'Ex consequuntur consequatur rem atque. Nesciunt omnis ut labore natus. Dolor quo voluptatem exercitationem ipsam minima.', NULL, 'qcollier@example.com');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (10, '2015-08-10 02:35:19',1, 'Inventore ut dolores modi harum eos unde veniam.', 'Labore velit aut praesentium aspernatur in. Ut velit qui error non error dolorum. Quam iusto laudantium libero nisi ut quia ipsam.', NULL, 'zbergnaum@example.org');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (11, '2014-04-09 01:19:44',2, 'Perspiciatis vero voluptatem et cumque.', 'Deserunt commodi animi magnam numquam asperiores et ex. Itaque aut iste et est veniam eveniet cum eligendi. Nemo ratione autem officia qui repellat est omnis quibusdam. Omnis sunt commodi error qui ex voluptatum laboriosam.', NULL, 'jalon07@example.org');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (12, '1971-08-01 10:02:27',2, 'Ab est reiciendis dolores repudiandae architecto dolorem.', 'Magnam velit eos quod labore quidem. Consequatur laudantium in perspiciatis delectus. Eos consequatur voluptate consectetur rerum perferendis quisquam et.', NULL, 'brown.guiseppe@example.org');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (13, '1974-01-29 02:27:06',2, 'Iusto sed voluptatem quia veritatis.', 'Est culpa velit quo magni. Necessitatibus earum deleniti ducimus. Accusamus consequatur consequatur sed.', NULL, 'iprosacco@example.com');
INSERT INTO `EMAIL` (`ID`, `DATE`,FOLDERKEY, `SUBJECT`, `MESSAGE`, `HTML`, `FROMWHO`) VALUES (14, '1981-04-06 09:24:48',2, 'Maxime commodi dolore dolores eum eveniet ex.', 'Ut dolor ipsum quibusdam ipsa sapiente. Numquam illo tempora omnis ea voluptatem odit iusto neque. Id dicta at voluptas minus maxime non. Et ut nobis debitis quod molestias dolor dolores.', NULL, 'kaitlin38@example.com');


INSERT INTO `FOLDER` (`ID`, `FOLDERNAME`) VALUES (0, 'inbox');
INSERT INTO `FOLDER` (`ID`, `FOLDERNAME`) VALUES (2, 'personal');
INSERT INTO `FOLDER` (`ID`, `FOLDERNAME`) VALUES (3, 'important');
INSERT INTO `FOLDER` (`ID`, `FOLDERNAME`) VALUES (4, 'deleted');
INSERT INTO `FOLDER` (`ID`, `FOLDERNAME`) VALUES (5, 'drafts');
INSERT INTO `FOLDER` (`ID`, `FOLDERNAME`) VALUES (6, 'outbox');



INSERT INTO `ADDRESSES` (`ID`, `ADDRESS`) VALUES (1, 'ellsworth.rowe@murray.com');
INSERT INTO `ADDRESSES` (`ID`, `ADDRESS`) VALUES (2, 'ibrahim02@gmail.com');
INSERT INTO `ADDRESSES` (`ID`, `ADDRESS`) VALUES (3, 'ethel.becker@bogan.com');
INSERT INTO `ADDRESSES` (`ID`, `ADDRESS`) VALUES (4, 'linnea.donnelly@heaney.net');
INSERT INTO `ADDRESSES` (`ID`, `ADDRESS`) VALUES (5, 'miles.anderson@gmail.com');
INSERT INTO `ADDRESSES` (`ID`, `ADDRESS`) VALUES (6, 'shammes@koeppwillms.biz');
INSERT INTO `ADDRESSES` (`ID`, `ADDRESS`) VALUES (7, 'xmayer@rempel.com');
INSERT INTO `ADDRESSES` (`ID`, `ADDRESS`) VALUES (8, 'lisandro16@heidenreichpfannerstill.com');


INSERT INTO `EMAILBRADDRESSES` (`TYPE`, `EMAILID`,`ADDRESSID`) VALUES ('CC', 1,1);
INSERT INTO `EMAILBRADDRESSES` (`TYPE`, `EMAILID`,`ADDRESSID`) VALUES ('CC', 1,2);
INSERT INTO `EMAILBRADDRESSES` (`TYPE`, `EMAILID`,`ADDRESSID`) VALUES ('CC', 1,3);
INSERT INTO `EMAILBRADDRESSES` (`TYPE`, `EMAILID`,`ADDRESSID`) VALUES ('BCC', 2,4);
INSERT INTO `EMAILBRADDRESSES` (`TYPE`, `EMAILID`,`ADDRESSID`) VALUES ('BCC', 2,5);
INSERT INTO `EMAILBRADDRESSES` (`TYPE`, `EMAILID`,`ADDRESSID`) VALUES ('TO', 3,6);


INSERT INTO BINARYFILES (`ID`, `FILENAME`,`BINARYDATA`) VALUES (0,'file.jpg',92939393);
INSERT INTO BINARYFILES (`ID`, `FILENAME`,`BINARYDATA`) VALUES (2,'file1.jpg',92939393);
INSERT INTO BINARYFILES (`ID`, `FILENAME`,`BINARYDATA`) VALUES (3,'file2.jpg',92939393);

INSERT INTO `EMAILBRBINARYFILES` (`TYPE`, `EMAILID`, `FILEID` ) VALUES ('EMBEDDED', 1,1);
INSERT INTO `EMAILBRBINARYFILES` (`TYPE`, `EMAILID`, `FILEID` ) VALUES ('REGULAR', 1,2);
INSERT INTO `EMAILBRBINARYFILES` (`TYPE`, `EMAILID`, `FILEID` ) VALUES ('REGULAR', 2,2);