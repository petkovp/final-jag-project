/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petko.test.sendandreceive;

import com.petko.business.SendAndReceive;
import com.petko.beans.MailConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javax.mail.SendFailedException;
import jodd.mail.Email;
import jodd.mail.ReceivedEmail;
import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Petko Petkov
 */

public class SendAndReceiveTest {

    private final Logger LOG = LoggerFactory.getLogger(getClass().getName());
    MailConfig sendConfig;
    MailConfig receiveConfig;
    MailConfig ccConfig;
    MailConfig bccConfig;
    SendAndReceive mailApp;
    String subject;
    String message;
    String html;

    @Before
    public void init() {
        LOG.info("tests staarted");
        sendConfig = new MailConfig("", "smtp.gmail.com", "smtp.gmail.com", "993", "465", "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", "MAILAPP", "3306", "mailappuser", "petkostandard", "sender.email13@gmail.com", "Dawson123@");
        receiveConfig = new MailConfig("", "smtp.gmail.com", "smtp.gmail.com", "993", "465", "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", "MAILAPP", "3306", "mailappuser", "petkostandard", "receiver.email12@gmail.com", "Dawson123@");
        ccConfig = new MailConfig("", "smtp.gmail.com", "smtp.gmail.com", "993", "465", "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", "MAILAPP", "3306", "mailappuser", "petkostandard", "cc.receiver14@gmail.com", "Dawson123@");
        bccConfig = new MailConfig("", "smtp.gmail.com", "smtp.gmail.com", "993", "465", "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", "MAILAPP", "3306", "mailappuser", "petkostandard", "bcc.receiver15@gmail.com", "Dawson123@");
        
        //sendConfig = new MailConfig("smtp.gmail.com", "sender.email13@gmail.com", "Dawson123@");
        //receiveConfig = new MailConfig("smtp.gmail.com", "receiver.email12@gmail.com", "Dawson123@");
        //ccConfig = new MailConfig("smtp.gmail.com", "cc.receiver14@gmail.com", "Dawson123@");
        //bccConfig = new MailConfig("smtp.gmail.com", "bcc.receiver15@gmail.com", "Dawson123@");

        subject = "This is a subject";
        message = "Just a normal message";
        html = "<html><META http-equiv=Content-Type "
                + "content=\"text/html; charset=utf-8\">"
                + "<body><h1>Here is my photograph embedded in "
                + "this email.</h1><img src='cid:FreeFall.jpg'>"
                + "<h2>I'm flying!</h2></body></html>";

    }

    /**
     * tests if a single email is sent to the "to" email
     *
     */
    @Test
    public void toSendSingleEmailTest() {
        mailApp = new SendAndReceive();
        ArrayList<String> toList = new ArrayList<>();
        toList.add("receiver.email12@gmail.com");
        //empty cclist and bcclist because only testing "to" reveiver
        ArrayList<String> ccList = new ArrayList<>();
        ArrayList<String> bccList = new ArrayList<>();

        ArrayList<File> embeddedFiles = new ArrayList<>();
        ArrayList<File> regularFiles = new ArrayList<>();
        regularFiles.add(new File("FreeFall.jpg"));
        embeddedFiles.add(new File("WindsorKen180.jpg"));

        try {
            Email sentEmail = mailApp.sendEmail(sendConfig, toList, ccList, bccList, subject, message, html, regularFiles, embeddedFiles);
            Thread.sleep(2000);
            ReceivedEmail[] receivedEmail = mailApp.receiveEmail(receiveConfig);

            String problems = "These fields were incorrect:";
            boolean test = true;

            if (receivedEmail == null) {
                test = false;
                Assert.fail(" no new messages");
            } else {
                LOG.info(receivedEmail[0].toString());
                if (!receivedEmail[0].from().toString().equals(sendConfig.getUserEmailAddress())) {
                    problems += "From";
                    LOG.error("the sender was not retrieved from received email");
                    test = false;
                }
                if (!receivedEmail[0].subject().equals(sentEmail.subject())) {
                    problems += "Subject";
                    LOG.error("subjects differ");

                    test = false;
                }
                if (receivedEmail[0].attachments().get(0).equals(sentEmail.attachments().get(0))) {
                    problems += " attachment,";
                    LOG.error("attachements differ");

                    test = false;
                }

                for (int i = 0; i < receivedEmail[0].messages().size(); i++) {
                    if (receivedEmail[0].messages().get(i).equals(sentEmail.messages().get(i))) {
                        problems += " message,";
                        LOG.error("messages differ");

                        test = false;
                    }
                }
//                
                assertTrue(problems, test);
            }

        } catch (InterruptedException e) {
            LOG.error("Threaded sleep failed", e);
        } catch (SendFailedException ex) {
            LOG.error("message was not sent", ex);
        } catch (FileNotFoundException ex) {
            LOG.error("attachment not found", ex);
        }
    }

    /**
     * tests if a single email is sent to the "to" email
     *
     */
    @Test
    public void ccSendSingleEmailTest() {
        mailApp = new SendAndReceive();
        ArrayList<String> ccList = new ArrayList<>();
        ccList.add("cc.receiver14@gmail.com");

//empty tolist and bcclist because only testing "cc" reveiver
        ArrayList<String> toList = new ArrayList<>();
        ArrayList<String> bccList = new ArrayList<>();
        ArrayList<File> embeddedFiles = new ArrayList<>();
        ArrayList<File> regularFiles = new ArrayList<>();
        regularFiles.add(new File("FreeFall.jpg"));
        embeddedFiles.add(new File("WindsorKen180.jpg"));
        try {

            Email sentEmail = mailApp.sendEmail(sendConfig, toList, ccList, bccList, subject, message, html, regularFiles, embeddedFiles);
            Thread.sleep(2000);
            ReceivedEmail[] receivedEmail = mailApp.receiveEmail(ccConfig);
            String problems = "These fields were incorrect:";
            boolean test = true;
            if (receivedEmail == null) {
                test = false;
                LOG.info("no receive emails retrieved");
                Assert.fail(" no new messages");
            } else {

                if (!receivedEmail[0].from().toString().equals(sendConfig.getUserEmailAddress())) {
                    problems += "From";
                    LOG.error("the sender was not retrieved from received email");
                    test = false;
                }
                if (!receivedEmail[0].subject().equals(sentEmail.subject())) {
                    problems += "Subject";
                    LOG.error("subjects differ");

                    test = false;
                }
                if (receivedEmail[0].attachments().get(0).equals(sentEmail.attachments().get(0))) {
                    problems += " attachment,";
                    LOG.error("attachements differ");

                    test = false;
                }

                for (int i = 0; i < receivedEmail[0].messages().size(); i++) {
                    if (receivedEmail[0].messages().get(i).equals(sentEmail.messages().get(i))) {
                        problems += " message,";
                        LOG.error("messages differ");

                        test = false;
                    }
                }
//                
                assertTrue(problems, test);
            }

        } catch (InterruptedException e) {
            LOG.error("Threaded sleep failed", e);
            Assert.fail();
        } catch (SendFailedException ex) {
            LOG.error("message was not sent", ex);
            Assert.fail();
        } catch (FileNotFoundException ex) {
            LOG.error("attachment not found", ex);
            Assert.fail();
        }

    }

    /**
     * sending email to many bcc recipients, checks if email is received by the
     * first recipient and check if the other recipients are visible fails if
     * bcc can see other recipients
     */
    @Test
    public void bccSendSingleEmailTest() {
        mailApp = new SendAndReceive();
        ArrayList<String> bccList = new ArrayList<>();
        bccList.add("bcc.receiver15@gmail.com");
        bccList.add("bcc.receiver15@gmail.com");
//empty tolist and cclist because only testing "bcc" 

        ArrayList<String> toList = new ArrayList<>();
        ArrayList<String> ccList = new ArrayList<>();
        ArrayList<File> embeddedFiles = new ArrayList<>();
        ArrayList<File> regularFiles = new ArrayList<>();
        regularFiles.add(new File("FreeFall.jpg"));
        embeddedFiles.add(new File("WindsorKen180.jpg"));
        try {

            Email sentEmail = mailApp.sendEmail(sendConfig, toList, ccList, bccList, subject, message, html, regularFiles, embeddedFiles);
            Thread.sleep(2000);
            ReceivedEmail[] receivedEmail = mailApp.receiveEmail(bccConfig);
            String problems = "These fields were incorrect:";
            boolean test = true;
            if (receivedEmail == null) {
                test = false;
                LOG.info("no receive emails retrieved");
                Assert.fail(" no new messages");
            } else {

                if (!receivedEmail[0].from().toString().equals(sendConfig.getUserEmailAddress())) {
                    problems += "From";
                    LOG.error("the sender was not retrieved from received email from:" + receivedEmail[0].from().toString());
                    test = false;
                }
                //checks if more than one receiver is visible in the received email
                if (receivedEmail[0].to().length != 0) {
                    problems += "bcc receivers are visible";
                    LOG.error("bcc receivers");
                    test = false;
                }
                if (!receivedEmail[0].subject().equals(sentEmail.subject())) {
                    problems += "Subject";
                    LOG.error("subjects differ");

                    test = false;
                }
                if (receivedEmail[0].attachments().get(0).equals(sentEmail.attachments().get(0))) {
                    problems += " attachment,";
                    LOG.error("attachements differ");

                    test = false;
                }

                for (int i = 0; i < receivedEmail[0].messages().size(); i++) {
                    if (receivedEmail[0].messages().get(i).equals(sentEmail.messages().get(i))) {
                        problems += " message,";
                        LOG.error("messages differ");

                        test = false;
                    }
                }
//                
                assertTrue(problems, test);
            }

        } catch (InterruptedException e) {
            LOG.error("Threaded sleep failed", e);
            Assert.fail();
        } catch (SendFailedException ex) {
            LOG.error("message was not sent", ex);
            Assert.fail();
        } catch (FileNotFoundException ex) {
            LOG.error("attachment not found", ex);
            Assert.fail();
        }

    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
     @Test (expected = FileNotFoundException.class)
    public void nonExistingEmbeddedAttachementTest() throws FileNotFoundException, SendFailedException {
        mailApp = new SendAndReceive();
        ArrayList<String> toList = new ArrayList<>();
        toList.add("receiver.email12@gmail.com");
        //empty cclist and bcclist because only testing "to" reveiver
        ArrayList<String> ccList = new ArrayList<>();
        ArrayList<String> bccList = new ArrayList<>();

        ArrayList<File> embeddedFiles = new ArrayList<>();
        ArrayList<File> regularFiles = new ArrayList<>();

        embeddedFiles.add(new File("unknown.jpg"));

        Email sentEmail = mailApp.sendEmail(sendConfig, toList, ccList, bccList, subject, message, html, regularFiles, embeddedFiles);

    }

    /**
     *
     * @throws FileNotFoundException
     * @throws SendFailedException
     */
   
    @Test(expected = FileNotFoundException.class)
    public void nonExistingRegulatAttachementTest() throws FileNotFoundException, SendFailedException {
        mailApp = new SendAndReceive();
        ArrayList<String> toList = new ArrayList<>();
        toList.add("receiver.email12@gmail.com");
        //empty cclist and bcclist because only testing "to" reveiver
        ArrayList<String> ccList = new ArrayList<>();
        ArrayList<String> bccList = new ArrayList<>();

        ArrayList<File> embeddedFiles = new ArrayList<>();
        ArrayList<File> regularFiles = new ArrayList<>();

        regularFiles.add(new File("unknown.jpg"));

        Email sentEmail = mailApp.sendEmail(sendConfig, toList, ccList, bccList, subject, message, html, regularFiles, embeddedFiles);

    }

    /**
     * checks if exception if thrown if the sender credentials are invalid
     *
     * @throws FileNotFoundException
     * @throws SendFailedException
     */
    @Test(expected = SendFailedException.class)
    public void wrongCredentionalsSendTest() throws FileNotFoundException, SendFailedException {
        mailApp = new SendAndReceive();
        ArrayList<String> toList = new ArrayList<>();
        toList.add("receiver.email12@gmail.com");
        //empty cclist and bcclist because only testing "to" reveiver
        ArrayList<String> ccList = new ArrayList<>();
        ArrayList<String> bccList = new ArrayList<>();

        ArrayList<File> embeddedFiles = new ArrayList<>();
        ArrayList<File> regularFiles = new ArrayList<>();

        MailConfig invalidConfig = new MailConfig("", "smtp.gmail.com", "smtp.gmail.com", "993", "465", "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", "MAILAPP", "3306", "mailappuser", "petkostandard", "ThisIsAnInvalidEMAIL", "Dawson123@");
        
        Email sentEmail = mailApp.sendEmail(invalidConfig, toList, ccList, bccList, subject, message, html, regularFiles, embeddedFiles);

    }

    /**
     * checks if exception is thrown if there is no receiver
     *
     * @throws FileNotFoundException
     * @throws SendFailedException
     */
    @Test(expected = SendFailedException.class)
    public void noReceiverSendTest() throws FileNotFoundException, SendFailedException {
        mailApp = new SendAndReceive();
        ArrayList<String> toList = new ArrayList<>();
        //empty cclist and bcclist because only testing "to" reveiver
        ArrayList<String> ccList = new ArrayList<>();
        ArrayList<String> bccList = new ArrayList<>();

        ArrayList<File> embeddedFiles = new ArrayList<>();
        ArrayList<File> regularFiles = new ArrayList<>();

        Email sentEmail = mailApp.sendEmail(sendConfig, toList, ccList, bccList, subject, message, html, regularFiles, embeddedFiles);

    }

    /**
     * send 5 emails and checks if five emails are received
     *
     */
    @Test
    public void receiveMethodTest() throws SendFailedException {
        mailApp = new SendAndReceive();
        ArrayList<String> bccList = new ArrayList<>();
        bccList.add("bcc.receiver15@gmail.com");

        //empty tolist and cclist because only testing "bcc" 
        ArrayList<String> toList = new ArrayList<>();
        ArrayList<String> ccList = new ArrayList<>();
        ArrayList<File> embeddedFiles = new ArrayList<>();
        ArrayList<File> regularFiles = new ArrayList<>();

        //makes sure that there are no unseen messages before testing
        ReceivedEmail[] receivedEmails = mailApp.receiveEmail(bccConfig);

        try {
            for (int i = 0; i < 5; i++) {
                Email sentEmail = mailApp.sendEmail(sendConfig, toList, ccList, bccList, subject + i, message, html, regularFiles, embeddedFiles);
                Thread.sleep(2000);
            }

            receivedEmails = mailApp.receiveEmail(bccConfig);
            //gets newEmails
            //if there are 5 new emails==true
            LOG.info("the new length is:" + receivedEmails.length);
            assertTrue("sls", receivedEmails.length == 5);

        } catch (InterruptedException e) {
            LOG.error("Threaded sleep failed", e);
            Assert.fail();
        } catch (SendFailedException ex) {
            LOG.error("message was not sent", ex);
            Assert.fail();
        } catch (FileNotFoundException ex) {
            LOG.error("attachment not found", ex);
            Assert.fail();
        }

    }

    @Test(expected = SendFailedException.class)
    public void invalidReceiveConfigBeanTest() throws SendFailedException {
        MailConfig invalidConfig = new MailConfig("", "smtp.gmail.com", "smtp.gmail.com", "993", "465", "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", "MAILAPP", "3306", "mailappuser", "petkostandard", "ThisIsAnInvalidEMAIL", "Dawson123@");
        mailApp = new SendAndReceive();
        ReceivedEmail[] receivedEmails = mailApp.receiveEmail(invalidConfig);
    }

}
