package com.petko.test.DAO;

import com.petko.beans.AddressData;
import com.petko.beans.AttachmentData;
import com.petko.beans.EmailData;
import com.petko.beans.FolderData;
import com.petko.beans.MailConfig;
import com.petko.business.SendAndReceive;
import com.petko.persistence.AddressesDAO;
import com.petko.persistence.AddressesDAOImpl;
import com.petko.persistence.AttachmentsDAO;
import com.petko.persistence.AttachmentsDAOImpl;
import com.petko.persistence.EmailDAO;
import com.petko.persistence.EmailDAOImpl;
import com.petko.persistence.FolderDAO;
import com.petko.persistence.FolderDAOImpl;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javafx.collections.ObservableList;

import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import org.junit.After;
import org.junit.AfterClass;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A basic unit test
 *
 * @author Ken Fogel
 * @version 2.0
 */

public class DAOTestCase {

    private final static Logger LOG = LoggerFactory.getLogger(DAOTestCase.class);

    private MailConfig databaseConfig;
    private MailConfig invalidConfig;

    /**
     *
     * this test is done once per DAO class to check is the validateCredentials
     * method works properly ValidateCredentials is called at the beginning of
     * every public DAO Class method now testing email DAO class
     */
    @Test(expected = SQLClientInfoException.class)
    public void emailDAOCredentialsValidationTest() throws SQLException {
        EmailData emailData = new EmailData();
        String htmlmsg = "<html></html>";
        emailData.setId(222);

        Email emailObject = Email.create();
        emailObject.from("gmail@gmail.com").htmlMessage(htmlmsg).textMessage("hello").subject("subject");
        emailData.setEmail(emailObject);

        emailData.setHtmlMessage(htmlmsg);
        emailData.setRegularMessage("hello");
        emailData.setFolderKey(1);

        EmailDAO ed = new EmailDAOImpl();

        ed.create(invalidConfig, emailData);

    }

    /**
     *
     * this test is done once per DAO class to check is the validateCredentials
     * method works properly ValidateCredentials is called at the beginning of
     * every public DAO Class method now testing folder DAO
     *
     * @throws java.sql.SQLException
     */
    @Test(expected = SQLClientInfoException.class)
    public void folderDAOCredentialsValidationTest() throws SQLException {
        FolderData folderData = new FolderData();
        folderData.setId(0);
        folderData.setName("folder 1");

        FolderDAO fd = new FolderDAOImpl();
        fd.create(invalidConfig, folderData);

    }

    /**
     *
     * this test is done once per DAO class to check is the validateCredentials
     * method works properly ValidateCredentials is called at the beginning of
     * every public DAO Class method now testing Addresses DAO
     */
    @Test(expected = SQLClientInfoException.class)
    public void addressesDAOCredentialsValidationTest() throws SQLException {
        AddressData addressData = new AddressData();
        addressData.setAddress("testing@gmail.com");
        addressData.setEmailId(1);
        addressData.setType("cc");
        AddressesDAO ad = new AddressesDAOImpl();
        ad.create(invalidConfig, addressData);

    }

    /**
     * Test to determine if a Email record is successfully inserted into the
     * table
     *
     * @throws SQLException
     */
    @Test
    public void createEmailTest() throws SQLException {
        EmailData emailData = new EmailData();
        String htmlmsg = "<html></html>";
        emailData.setId(222);

        Email emailObject = Email.create();
        emailObject.from("gmail@gmail.com").htmlMessage(htmlmsg).textMessage("hello").subject("subject");
        emailObject.cc("cc.test123@gmail.com");
        emailObject.bcc("bcc.test123@gmail.com");
        emailObject.to("to.test123@gmail.com");

        emailData.setEmail(emailObject);
        emailData.setHtmlMessage(htmlmsg);
        emailData.setRegularMessage("hello");
        emailData.setFolderKey(1);

        EmailDAO ed = new EmailDAOImpl();

        ed.create(databaseConfig, emailData);

        EmailData emailData2 = ed.find(databaseConfig, 15);
        EmailAddress[] ccAddresses = emailData2.getEmail().cc();
        for (EmailAddress em : ccAddresses) {
            LOG.debug("cc  :" + em.getEmail());
        }
//        LOG.debug("id:"+emailData.getId());
//        LOG.debug("html:"+emailData.getHtmlMessage());
//        LOG.debug("reg msg" +emailData.getRegularMessage());
//        LOG.debug("from:"+emailData.email.from());
//        LOG.debug("subj:"+emailData.email.subject());
//        LOG.debug("folderkey:"+emailData.getFolderKey());
//        LOG.debug(":::::email 2:::::::");
//         LOG.debug("id:"+emailData2.getId());
//        LOG.debug("html:"+emailData2.getHtmlMessage());
//        LOG.debug("reg msg" +emailData2.getRegularMessage());
//        LOG.debug("from:"+emailData2.email.from());
//        LOG.debug("subj:"+emailData2.email.subject());
//        LOG.debug("folderkey:"+emailData2.getFolderKey());
        assertEquals("Create Test: ", emailData, emailData2);
    }

    /**
     * checks if findAllEmailContaining successfully retrieves the created email
     * which contains a given string
     *
     * @throws SQLException
     */
    @Test
    public void findAllEmailsContainingTest() throws SQLException {
        EmailData email1 = new EmailData();
        String htmlmsg = "<html>hellohellohellohellohello</html>";
        email1.setId(222);

        Email emailObject = Email.create();
        emailObject.from("gmail@gmail.com").htmlMessage(htmlmsg).textMessage("hello").subject("subject");
        email1.setEmail(emailObject);

        email1.setHtmlMessage(htmlmsg);
        email1.setRegularMessage("hello");
        email1.setFolderKey(1);

        EmailDAO ed = new EmailDAOImpl();
        ed.create(databaseConfig, email1);
        List<EmailData> foundEmails = ed.findAllContaining(databaseConfig, "hellohellohello");

        assertEquals("FIND ALL EMAILS CONTAINING TEST: ", 1, foundEmails.size());
    }

    /**
     * checks if an email is successfully deleted
     *
     * @throws SQLException
     */
    @Test
    public void DeleteEmailTest() throws SQLException {
        EmailData email1 = new EmailData();
        String htmlmsg = "<html>hellohellohellohellohello</html>";
        email1.setId(15);

        Email emailObject = Email.create();
        emailObject.from("gmail@gmail.com").htmlMessage(htmlmsg).textMessage("hello").subject("subject");
        email1.setEmail(emailObject);

        email1.setHtmlMessage(htmlmsg);
        email1.setRegularMessage("hello");
        email1.setFolderKey(1);

        EmailDAO ed = new EmailDAOImpl();
        ed.create(databaseConfig, email1);
        ed.delete(databaseConfig, email1.getId());
        List<EmailData> foundEmails = ed.findAllContaining(databaseConfig, "hellohellohello");

        assertEquals("FIND ALL EMAILS CONTAINING TEST: ", 0, foundEmails.size());
    }

    /**
     * checks if an email is successfully updated in the database when an
     * EmailData object has been modified
     *
     * @throws SQLException
     */
    @Test
    public void UpdateEmailTest() throws SQLException {
        EmailData email1 = new EmailData();
        String htmlmsg = "<html>hellohellohellohellohello</html>";
        email1.setId(15);

        Email emailObject = Email.create();
        emailObject.from("gmail@gmail.com").htmlMessage(htmlmsg).textMessage("hello").subject("subject");
        email1.setEmail(emailObject);

        email1.setHtmlMessage(htmlmsg);
        email1.setRegularMessage("hello");
        email1.setFolderKey(1);

        EmailDAO ed = new EmailDAOImpl();
        ed.create(databaseConfig, email1);
        email1.email.subject("modified subject");
        ed.update(databaseConfig, email1);
        List<EmailData> foundEmails = ed.findAllContaining(databaseConfig, "modified subject");

        assertEquals("update email TEST: ", 1, foundEmails.size());
    }

    /**
     * Checks if the findAll retrieves all the initially created emails
     *
     * @throws SQLException
     */
    @Test
    public void FindAllEmailsTest() throws SQLException {
        EmailDAO ed = new EmailDAOImpl();
        List<EmailData> foundEmails = ed.findAll(databaseConfig);

        assertEquals("FIND ALL EMAILS TEST: ", 14, foundEmails.size());
    }

    /**
     * checks if a folder is successfully created by retrieving all the folders
     * and by checking if the created folder is contained in this list
     *
     * @throws SQLException
     */
    @Test
    public void folderCreationTest() throws SQLException {
        FolderData folderData = new FolderData();
        folderData.setId(0);
        folderData.setName("folder 1");

        FolderDAO fd = new FolderDAOImpl();
        fd.create(databaseConfig, folderData);
        ObservableList<FolderData> folderList = fd.findAll(databaseConfig);
        for (FolderData fdd : folderList) {
            LOG.debug(fdd.getName());
        }
        assertTrue(folderList.contains(folderData));

    }

    /**
     * tests if an address record is successfully created and if the bridge
     * table is filled accordingly
     *
     * @throws SQLException
     */
    @Test
    public void addressCreateTest() throws SQLException {
        AddressData addressData = new AddressData();
        addressData.setAddress("testing@gmail.com");
        addressData.setEmailId(1);
        addressData.setType("cc");
        AddressesDAO ad = new AddressesDAOImpl();
        ad.create(databaseConfig, addressData);

        List<AddressData> adList = ad.findAll(databaseConfig);

        assertTrue(adList.contains(addressData));

    }

    /**
     * tests if an attachment record is successfully created 
     *
     * @throws SQLException
     */
    @Test
    public void attachmentCreateTest() throws SQLException {
        AttachmentData attachmentData = new AttachmentData();
        attachmentData.setEmailId(1);
        attachmentData.setFileName("test.jpg");
        attachmentData.setIsEmbedded(true);
        byte b[];
        //MEMORY ALLOCATION FOR JAVA BYTE ARRAY
        b = new byte[4];
        //ASSIGNING ELEMENTS TO JAVA BYTE ARRAY
        b[0] = 20;b[1] = 10;b[2] = 30;b[3] = 5;
        attachmentData.setPicture(b);

        AttachmentsDAO attachmentsManager  = new AttachmentsDAOImpl();
        int created = attachmentsManager.create(databaseConfig, attachmentData);
        
        //List<AttachmentData> retrievedAttachments = attachmentsManager.findAttachmentsForEmail(databaseConfig, 33);
        
       

       

        assertEquals(1,created);

    }
     /**
     * tests if an attachment record is successfully created from email data
     *
     * @throws SQLException
     */
    @Test
    public void saveAttachmentsForEmailTest() throws SQLException {
        EmailData emailData = new EmailData();
        String htmlmsg = "<html></html>";
        emailData.setId(222);

        Email emailObject = Email.create();
        emailObject.from("gmail@gmail.com").htmlMessage(htmlmsg).textMessage("hello").subject("subject");
        emailObject.cc("cc.test123@gmail.com");
        emailObject.bcc("bcc.test123@gmail.com");
        emailObject.to("to.test123@gmail.com");
        
        
        emailObject.attachment(EmailAttachment.with().content(new File("FreeFall.jpg")));
        emailObject.embeddedAttachment(EmailAttachment.with().content(new File("WindsorKen180.jpg")));
        emailData.setEmail(emailObject);
        emailData.setHtmlMessage(htmlmsg);
        emailData.setRegularMessage("hello");
        emailData.setFolderKey(1);

        EmailDAO ed = new EmailDAOImpl();
        ed.create(databaseConfig, emailData);
        
        AttachmentsDAO attachmentsManager  = new AttachmentsDAOImpl();
        int retrieved = attachmentsManager.addAttachmentsToEmail(databaseConfig, emailData);
       
        //List<AttachmentData> retrievedAttachments = attachmentsManager.findAttachmentsForEmail(databaseConfig, 33);
        
       

       

        assertEquals(2,retrieved);

    }
    
    

    /**
     * The database is recreated before each test. If the last test is
     * destructive then the database is in an unstable state. @AfterClass is
     * called just once when the test class is finished with by the JUnit
     * framework. It is instantiating the test class anonymously so that it can
     * execute its non-static seedDatabase routine.
     */
    @Ignore
    public  void seedAfterTestCompleted() {
        LOG.info("@AfterClass seeding");
        new DAOTestCase().seedDatabase();
    }

    @Before
    public void init() {
        databaseConfig = new MailConfig();
        String url = "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
        String user = "mailappuser";
        String pass = "petkostandard";
        databaseConfig.setDatabaseName("MAILAPP");
        databaseConfig.setDatabaseUsername(user);
        databaseConfig.setDatabaseUrl(url);
        databaseConfig.setDatabasePassword(pass);

        invalidConfig = new MailConfig();
        invalidConfig.setDatabaseName("MAILAPP");
        invalidConfig.setDatabaseUsername(user);
        invalidConfig.setDatabaseUrl(url);
        invalidConfig.setDatabasePassword("invalidpasssss");
    }

    /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     */
    @Before
    public void seedDatabase() {

        LOG.info("@Before seeding");

        final String seedDataScript = loadAsString("createMailAppTables.sql");
        try ( Connection connection = DriverManager.getConnection(databaseConfig.getDatabaseUrl(), databaseConfig.getDatabaseUsername(), databaseConfig.getDatabasePassword());) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();

            }
            LOG.info("seeded successfully");
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabse method
     */
    private String loadAsString(final String path) {
        try ( InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);  Scanner scanner = new Scanner(inputStream)) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader, String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
    }

}
