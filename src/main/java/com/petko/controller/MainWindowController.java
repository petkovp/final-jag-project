package com.petko.controller;

import com.petko.beans.FolderData;
import com.petko.business.PropertiesManager;
import com.petko.beans.PropertyBean;
import com.petko.javamailapp.MainApp;
import com.petko.persistence.EmailDAO;
import com.petko.persistence.EmailDAOImpl;
import com.petko.persistence.FolderDAO;
import com.petko.persistence.FolderDAOImpl;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainWindowController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="leftLayout"
    private BorderPane leftLayout; // Value injected by FXMLLoader

    @FXML // fx:id="upperRightLayout"
    private AnchorPane upperRightLayout; // Value injected by FXMLLoader

    @FXML // fx:id="lowerRightLayout"
    private AnchorPane lowerRightLayout; // Value injected by FXMLLoader

    private FolderTreeController folderTreeController;
    private EmailTableController emailTableController;
     private Parent rootPane;
    private EditorController editorController;
    @FXML
    private MenuItem editConfig;

    private final static Logger LOG = LoggerFactory.getLogger(MainWindowController.class);

    private FolderDAO folderDao;
    private EmailDAO emailDao;
    private PropertiesManager pm;
    private PropertyBean propertyBean;

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        

        folderDao = new FolderDAOImpl();
        emailDao = new EmailDAOImpl();
        try {
            initLeftLayout();
            initUpperRightLayout();
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }

        initLowerRightLayout();
        setTableControllerToTree();

        try {
            
        } catch (/*SQLException |*/Exception ex) {
            LOG.error("initialize error", ex);
            errorAlert("initialize()");
            Platform.exit();
        }
    }

    /**
     * inits folder tree
     * @throws SQLException 
     */
    private void initLeftLayout() throws SQLException {
        try {
            FXMLLoader loader = new FXMLLoader();

            loader.setResources(ResourceBundle.getBundle("MessagesBundle"));

            loader.setLocation(MainWindowController.class
                    .getResource("/fxml/FolderTree.fxml"));
            BorderPane treeView = (BorderPane) loader.load();

            // Give the controller the data object.
            folderTreeController = loader.getController();
            folderTreeController.setFolderDAO(folderDao);
            folderTreeController.setEmailDAO(emailDao);
            folderTreeController.displayTree();

            leftLayout.getChildren().add(treeView);
            LOG.info("Left layout initiated");
        } catch (IOException ex) {
            LOG.error("initUpperLeftLayout error", ex);
            LOG.error("initUpperLeftLayout()");
            Platform.exit();
        }

    }
    /**
     * inits email table 
     * @throws SQLException 
     */
    private void initUpperRightLayout() throws SQLException {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(MainWindowController.class
                    .getResource("/fxml/EmailTable.fxml"));
            BorderPane tableView = (BorderPane) loader.load();

            // Give the controller the data object.
            emailTableController = loader.getController();
            emailTableController.setEmailDao(emailDao);
            emailTableController.displayTheTable();
            
            upperRightLayout.getChildren().add(tableView);
            LOG.info("Upper right layout initiated");
        } catch (/*SQLException |*/IOException ex) {
            LOG.error("initUpperRightLayout error", ex);
            errorAlert("initUpperRightLayout()");
            Platform.exit();
        }
    }
     
    /**
     * inits the html editor
     */
    private void initLowerRightLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);

            loader.setLocation(MainWindowController.class
                    .getResource("/fxml/HtmlEditor.fxml"));
            BorderPane htmlView = (BorderPane) loader.load();

            // Give the controller the data object.
            editorController = loader.getController();
            emailTableController.setEditorController(editorController);
            //   editorController.setFishDAO(fishDAO);
            editorController.setEmailDAO(emailDao);
            lowerRightLayout.getChildren().add(htmlView);
            LOG.info("Lower left layout initiated");
        } catch (IOException ex) {
            LOG.error("initLowerRightLayout error", ex);
            errorAlert("initLowerRightLayout()");
            Platform.exit();
        }
    }

    @FXML
    void editConfigClicked(ActionEvent event) {
        
            showPropertiesLayout();
            
            
        

    }
    /**
     * displays porperties form
     */
    public void showPropertiesLayout() {
        try {
            retrieveMailConfig();
            // Instantiate a FXMLLoader object
            FXMLLoader loader = new FXMLLoader();

            // Configure the FXMLLoader with the i18n locale resource bundles
            loader.setResources(ResourceBundle.getBundle("MessagesBundle", Locale.getDefault()));

            // Connect the FXMLLoader to the fxml file that is stored in the jar
            loader.setLocation(MainApp.class
                    .getResource("/fxml/PropertiesForm.fxml"));

           
             
            // The load command returns a reference to the root pane of the fxml file
            rootPane = (GridPane) loader.load();

            // Retreive a refernce to the controller from the FXMLLoader
            PropertiesFormController controller = loader.getController();

            // You can now call on methods in the controller, usually to provide
            // supplemental information
            
            controller.setupProperties(pm, propertyBean);

            // Instantiate the scene with the root layout.
            Scene scene = new Scene(rootPane);
            Stage stage = new Stage();
             stage.setTitle("JAG");
             stage.setScene(scene);
             stage.show();
            // Put the Scene on the Stage
           // primaryStage.setScene(scene);

        } catch (IOException ex) {
            LOG.error("Error displaying form", ex);
            errorAlert(ex.getMessage());
        }

    }
    /**
     * sets propertymanager and property bean fields
     * @throws IOException 
     */
    private void retrieveMailConfig() throws IOException {
        pm = new PropertiesManager();
        propertyBean = new PropertyBean();
        pm.loadTextProperties(propertyBean, "", "MailConfig");

        LOG.debug(propertyBean.toString());
    }
   
    @FXML
    void handleHelp(ActionEvent event) {
        FXMLLoader loader = new FXMLLoader();
         loader.setResources(ResourceBundle.getBundle("MessagesBundle", Locale.getDefault()));
         loader.setLocation(MainApp.class.getResource("/fxml/HelpWebView.fxml"));
        try {
            rootPane = (AnchorPane) loader.load();
        } catch (IOException ex) {
           
            java.util.logging.Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
 
            Scene scene = new Scene(rootPane);
            Stage stage = new Stage();
             stage.setTitle("JAG");
             stage.setScene(scene);
             stage.show();
           
        
    }
    /**
     * Send the reference to the FishFXTableController to the
     * FishFXTreeController
     */
    private void setTableControllerToTree() {
        folderTreeController.setTableController(emailTableController);
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }
}
