package com.petko.controller;

/**
 * Sample Skeleton for 'EmailTable.fxml' Controller Class
 */
import com.petko.beans.EmailData;
import com.petko.beans.FolderData;
import com.petko.beans.MailConfig;
import com.petko.business.PropertiesManager;
import com.petko.beans.PropertyBean;
import com.petko.persistence.EmailDAO;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import jodd.mail.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmailTableController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="tableLayout"
    private BorderPane tableLayout; // Value injected by FXMLLoader

    @FXML // fx:id="tableViewLayout"
    private TableView<EmailData> tableViewLayout; // Value injected by FXMLLoader

    @FXML // fx:id="fromColumn"
    private TableColumn<EmailData, String> fromColumn; // Value injected by FXMLLoader

    @FXML // fx:id="subjectColumn"
    private TableColumn<EmailData, String> subjectColumn; // Value injected by FXMLLoader

    @FXML // fx:id="dateColumn"
    private TableColumn<EmailData, String> dateColumn; // Value injected by FXMLLoader

    private EmailDAO emailDAO;

    private final static Logger LOG = LoggerFactory.getLogger(EmailTableController.class);
    private EditorController editorController;

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        adjustColumnWidths();

        fromColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getFromProperty()
        );
        subjectColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getSubjectProperty()
        );

        dateColumn.setCellValueFactory(cellData -> cellData.getValue()
                .getReceivedDateProperty()
        );

        tableViewLayout
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showEmailDetails(newValue));

        tableViewLayout.setOnDragDetected((MouseEvent event) -> {
            /* drag was detected, start drag-and-drop gesture */
            LOG.debug("onDragDetected");

            /* allow any transfer mode */
            Dragboard db = tableViewLayout.startDragAndDrop(TransferMode.ANY);

            /* put a string on dragboard */
            ClipboardContent content = new ClipboardContent();
            content.putString(Integer.toString(tableViewLayout.getSelectionModel().getSelectedItem().getId()));
            
            LOG.debug("drag id:"+ content.getString());
            db.setContent(content);

            event.consume();
        });
    }
    
    public void setEditorController(EditorController editor){
        this.editorController = editor;
        
    }

    /**
     * The FolderTreeController needs a reference to the this controller. With
     * that reference it can call this method to retrieve a reference to the
     * TableView and change its selection
     *
     * @return
     */
    public TableView<EmailData> getEmailDataTable() {
        return this.tableViewLayout;
    }

    private void adjustColumnWidths() {
        // Get the current width of the table
        double width = tableLayout.getPrefWidth();
        // Set width of each column
        fromColumn.setPrefWidth(width * .25);
        subjectColumn.setPrefWidth(width * .5);
        dateColumn.setPrefWidth(width * .25);

    }

    @FXML
    void dragDetected(MouseEvent event) {
        LOG.debug("onDragDetected");

        /* allow any transfer mode */
        Dragboard db = tableViewLayout.startDragAndDrop(TransferMode.ANY);

        /* put a string on dragboard */
        ClipboardContent content = new ClipboardContent();
        content.putString(Integer.toString(tableViewLayout.getSelectionModel().getSelectedItem().getId()));

        db.setContent(content);

        event.consume();
    }

    public void setEmailDao(EmailDAO emailDAO) throws SQLException {
        this.emailDAO = emailDAO;
    }

    /**
     * The table displays the email data
     *
     * @throws SQLException
     */
    public void displayTheTable() throws SQLException {
        // Add observable list data to the table
        tableViewLayout.setItems(this.emailDAO.findAll(getDBMailConfig()));

    }

    public void displayEmailsFromFolder(FolderData folderData) throws SQLException {
        // Add observable list data to the table

        tableViewLayout.setItems(this.emailDAO.findAllFromFolder(getDBMailConfig(), folderData));

    }

    /**
     * To be able to test the selection handler for the table, this method
     * displays the FishData object that corresponds to the selected row.
     *
     * @param fishData
     */
    private void showEmailDetails(EmailData selectedEmail) {
            //EmailData draggedEmail = this.emailDAO.find(getDBMailConfig(), emailId);
            System.out.println(selectedEmail.toString());
            this.editorController.displayEmailInfo(selectedEmail.getId());
            
    }

    /**
     * this method returns a db mail config
     *
     * @return
     */
    private MailConfig getDBMailConfig() {
        PropertiesManager pm = new PropertiesManager();
        PropertyBean propertyBean = new PropertyBean();
        try {
            pm.loadTextProperties(propertyBean, "", "MailConfig");
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return propertyBean.getMailConfig();
        
    }
}
