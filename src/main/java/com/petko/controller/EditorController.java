package com.petko.controller;

/**
 * Sample Skeleton for 'HtmlEditor.fxml' Controller Class
 */
import com.petko.beans.EmailData;
import com.petko.beans.MailConfig;
import com.petko.business.DBManager;
import com.petko.business.PropertiesManager;
import com.petko.business.SendAndReceive;
import com.petko.beans.PropertyBean;
import com.petko.persistence.EmailDAO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.HTMLEditor;
import javax.mail.SendFailedException;
import jodd.mail.Email;
import jodd.mail.RFC2822AddressParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EditorController {

    private final static Logger LOG = LoggerFactory.getLogger(EditorController.class);
    private EmailDAO emailDAO;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="editorLayout"
    private BorderPane editorLayout; // Value injected by FXMLLoader

    @FXML // fx:id="subjectField"
    private TextField subjectField; // Value injected by FXMLLoader

    @FXML // fx:id="toField"
    private TextField toField; // Value injected by FXMLLoader

    @FXML // fx:id="ccField"
    private TextField ccField; // Value injected by FXMLLoader

    @FXML // fx:id="bccField"
    private TextField bccField; // Value injected by FXMLLoader

    @FXML
    private Button sendBtn;

    @FXML
    private Button saveBtn;

    @FXML // fx:id="htmlEditor"
    private HTMLEditor htmlEditor; // Value injected by FXMLLoader
    private PropertyBean propertyBean;
    Alert alert = new Alert(AlertType.NONE);

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert editorLayout != null : "fx:id=\"editorLayout\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert subjectField != null : "fx:id=\"subjectField\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert toField != null : "fx:id=\"toField\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert ccField != null : "fx:id=\"ccField\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert bccField != null : "fx:id=\"bccField\" was not injected: check your FXML file 'HtmlEditor.fxml'.";
        assert htmlEditor != null : "fx:id=\"htmlEditor\" was not injected: check your FXML file 'HtmlEditor.fxml'.";

    }

    /**
     * saves the fields to the draft folder
     * @param event 
     */
    @FXML
    void handleSave(ActionEvent event) {
        validateFields();

        getDBMailConfig();
        DBManager manage = new DBManager(this.propertyBean);

        manage.handleReceivedEmails();

        if (validateFields()) {
            EmailData emailData = new EmailData();
            //drafts id
            emailData.setFolderKey(5);

            Email emailObj = new Email();
            addToFieldsToEmail(emailObj, this.toField.getText().replace(" ", "").split(","));
            addCcFieldsToEmail(emailObj, this.ccField.getText().replace(" ", "").split(","));
            addBccFieldsToEmail(emailObj, this.bccField.getText().replace(" ", "").split(","));
            emailObj.htmlMessage(this.htmlEditor.getHtmlText());
            emailObj.subject(this.subjectField.getText());
            emailObj.from(this.propertyBean.getEmailAddress());
            emailData.setReceivedDate(new Timestamp(new Date().getTime()));
            emailData.setEmail(emailObj);
            emailData.setHtmlMessage(this.htmlEditor.getHtmlText());
            emailData.setRegularMessage(this.htmlEditor.getHtmlText());

            List<String> toList = Arrays.asList(this.toField.getText().replace(" ", "").split(","));
            List<String> ccList = Arrays.asList(this.ccField.getText().replace(" ", "").split(","));
            List<String> bccList = Arrays.asList(this.bccField.getText().replace(" ", "").split(","));

            DBManager saver = new DBManager(this.propertyBean);
            saver.setMailConfigBean(propertyBean);

            saver.saveEmailTODB(emailData);
            alert.setAlertType(AlertType.INFORMATION);
            alert.setContentText("Email Saved Successfully");
            alert.show();

        }

    }

    /**
     * validates all the fields to send an email
     *
     * @return
     */
    private boolean validateFields() {
        if (!checkEmail(this.toField.getText().replace(" ", "")) && !this.toField.getText().isEmpty()) {
            String[] toAddresses = this.toField.getText().replace(" ", "").split(",");
            if (checkEmailList(toAddresses) == false) {
                alert.setAlertType(AlertType.ERROR);
                alert.setContentText("Enter  valid comma separated emails in the TO field");
                alert.show();
                return false;
            }

        }
        if (!checkEmail(this.ccField.getText().replace(" ", "")) && !this.ccField.getText().isEmpty()) {
            String[] toAddresses = this.ccField.getText().replace(" ", "").split(",");
            if (checkEmailList(toAddresses) == false) {
                alert.setAlertType(AlertType.ERROR);
                alert.setContentText("Enter  valid comma separated emails in the CC field");
                alert.show();
                return false;
            }

        }
        if (!checkEmail(this.bccField.getText().replace(" ", "")) && !this.bccField.getText().isEmpty()) {
            String[] toAddresses = this.bccField.getText().replace(" ", "").split(",");
            if (checkEmailList(toAddresses) == false) {
                alert.setAlertType(AlertType.ERROR);
                alert.setContentText("Enter valid coma separated BCC emails");
                alert.show();
                return false;
            }

        }
        return true;

    }

    /**
     * returns true if all emailsfrom arrayList are valid
     *
     * @param emails
     * @return
     */
    private boolean checkEmailList(String[] emails) {
        boolean valid = true;
        for (String s : emails) {
            if (checkEmail(s) == false) {
                return false;
            }

        }
        return valid;

    }

    /**
     * check if a string is a valid email 
     * @param address
     * @return 
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

    /**
     * handles send button, check if fields are empty, if yes it only gets received emails
     * @param event 
     */
    @FXML
    void handleSend(ActionEvent event) {
        getDBMailConfig();
        DBManager manage = new DBManager(this.propertyBean);

        manage.handleReceivedEmails();
        if (!allFieldsEmpty()) {
            if (validateFields()) {
                EmailData emailData = new EmailData();
                //outbox id
                emailData.setFolderKey(6);

                Email emailObj = new Email();
                addToFieldsToEmail(emailObj, this.toField.getText().replace(" ", "").split(","));
                addCcFieldsToEmail(emailObj, this.ccField.getText().replace(" ", "").split(","));
                addBccFieldsToEmail(emailObj, this.bccField.getText().replace(" ", "").split(","));
                emailObj.htmlMessage(this.htmlEditor.getHtmlText());
                emailObj.subject(this.subjectField.getText());
                emailObj.from(this.propertyBean.getEmailAddress());
                emailData.setReceivedDate(new Timestamp(new Date().getTime()));
                emailData.setEmail(emailObj);
                emailData.setHtmlMessage(this.htmlEditor.getHtmlText());
                emailData.setRegularMessage(this.htmlEditor.getHtmlText());

                List<String> toList = Arrays.asList(this.toField.getText().replace(" ", "").split(","));
                List<String> ccList = Arrays.asList(this.ccField.getText().replace(" ", "").split(","));
                List<String> bccList = Arrays.asList(this.bccField.getText().replace(" ", "").split(","));

                SendAndReceive sender = new SendAndReceive();

                try {
                    sender.sendEmail(this.propertyBean.getMailConfig(), toList, ccList, bccList, this.subjectField.getText(), this.htmlEditor.getHtmlText(), this.htmlEditor.getHtmlText(), null, null);
                    alert.setAlertType(AlertType.INFORMATION);
                    alert.setContentText("Email Sent successfully");
                    alert.show();
                    DBManager saver = new DBManager(this.propertyBean);
                    saver.setMailConfigBean(propertyBean);

                    saver.saveEmailTODB(emailData);

                } catch (SendFailedException ex) {
                    java.util.logging.Logger.getLogger(EditorController.class.getName()).log(Level.SEVERE, null, ex);
                    alert.setAlertType(AlertType.INFORMATION);
                    alert.setContentText("Email Not Sent, new emails received ");
                    alert.show();
                } catch (FileNotFoundException ex) {
                    java.util.logging.Logger.getLogger(EditorController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    /**
     * returns true if all fields are empty
     * @return 
     */
    private boolean allFieldsEmpty() {
        if (this.toField.getText().equals("") && this.ccField.getText().equals("") && this.bccField.getText().equals("") && this.subjectField.getText().equals("") && this.htmlEditor.getHtmlText().equals("")) {
            return true;
        }
        return false;
    }

    /**
     * adds array of "to" emails to an email object
     *
     * @param email
     * @param addresses
     */
    private void addToFieldsToEmail(Email email, String[] addresses) {
        for (String s : addresses) {
            email.to(s);
        }
    }

    /**
     * add cc emails to an object
     *
     * @param email
     * @param addresses
     */
    private void addCcFieldsToEmail(Email email, String[] addresses) {
        for (String s : addresses) {
            email.cc(s);
        }
    }

    /**
     * adds the bcc emails to an email Object
     *
     * @param email
     * @param addresses
     */
    private void addBccFieldsToEmail(Email email, String[] addresses) {
        for (String s : addresses) {
            email.bcc(s);
        }
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * EmailDAO object.
     *
     * @param fishDAO
     */
    public void setEmailDAO(EmailDAO emailDAO) {
        this.emailDAO = emailDAO;
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(resources.getString("sqlError"));
        dialog.setHeaderText(resources.getString("sqlError"));
        dialog.setContentText(resources.getString(msg));
        dialog.show();
    }

    /**
     * this method is used for testing purposes and will be removed for phase4
     *
     * @return
     */
    private MailConfig getDBMailConfig() {
        PropertiesManager pm = new PropertiesManager();
        this.propertyBean = new PropertyBean();
        try {
            pm.loadTextProperties(propertyBean, "", "MailConfig");
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return propertyBean.getMailConfig();

    }

    /**
     * displays the email info by the emailId
     * @param emailId 
     */
    void displayEmailInfo(int emailId) {
        try {
            EmailData email = this.emailDAO.find(getDBMailConfig(), emailId);
            ccField.setText(email.getCcProperty().get());
            bccField.setText(email.getBccProperty().get());
            toField.setText(email.getToProperty().get());
            htmlEditor.setHtmlText(email.getRegularMessage());
            subjectField.setText(email.getEmail().subject());
            saveBtn.setDisable(true);
        } catch (SQLException ex) {
            LOG.error("problem displaying email info in html editor");
        }
    }

    /**
     * resets all fields
     * @param event 
     */
    @FXML
    void handleNewEmail(ActionEvent event) {
        ccField.setText("");
        bccField.setText("");
        toField.setText("");
        htmlEditor.setHtmlText("");
        subjectField.setText("");
        saveBtn.setDisable(false);
    }
}
