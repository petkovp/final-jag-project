package com.petko.controller;

import com.petko.beans.EmailData;
import java.net.URL;
import javafx.scene.layout.BorderPane;
import com.petko.beans.FolderData;
import com.petko.beans.MailConfig;
import com.petko.business.PropertiesManager;
import com.petko.beans.PropertyBean;
import com.petko.persistence.EmailDAO;
import com.petko.persistence.FolderDAO;
import java.io.IOException;
import javafx.scene.control.TreeCell;

import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.text.Text;

public class FolderTreeController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="folderFXTreeLayout"
    private BorderPane folderFXTreeLayout; // Value injected by FXMLLoader

    @FXML // fx:id="folderFXTreeView"
    private TreeView<FolderData> folderFXTreeView; // Value injected by FXMLLoader

    private FolderDAO folderDAO;
    private EmailTableController emailTableController;
    private final static Logger LOG = LoggerFactory.getLogger(FolderTreeController.class);
    private EmailDAO emailDAO;

    Alert a = new Alert(AlertType.NONE);

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        FolderData rootFolder = new FolderData();
        rootFolder.setName("Folder");
        folderFXTreeView.setRoot(new TreeItem<>(rootFolder));

        folderFXTreeView.setCellFactory((e) -> new TreeCell<FolderData>() {
            @Override
            protected void updateItem(FolderData item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    setText(item.getName());
                    setGraphic(getTreeItem().getGraphic());
                } else {
                    setText("");
                    setGraphic(null);
                }
            }
        });

        folderFXTreeView.setOnDragDetected((MouseEvent event) -> {
            /* drag was detected, start drag-and-drop gesture */
            // LOG.debug("onDragDetected");

            /* allow any transfer mode */
            Dragboard db = folderFXTreeView.startDragAndDrop(TransferMode.ANY);

            /* put a string on dragboard */
            ClipboardContent content = new ClipboardContent();
            content.putString(folderFXTreeView.getSelectionModel().getSelectedItem().getValue().toString());

            db.setContent(content);

            event.consume();
        });

        assert folderFXTreeLayout != null : "fx:id=\"folderFXTreeLayout\" was not injected: check your FXML file 'FolderTree.fxml'.";
        assert folderFXTreeView != null : "fx:id=\"folderFXTreeView\" was not injected: check your FXML file 'FolderTree.fxml'.";

    }

    /**
     * This method prevents dropping the value on anything buts the
     * FXHTMLEditor,
     *
     * SceneBuilder writes the event as ActionEvent that you must change to the
     * proper event type that in this case is DragEvent
     *
     * @param event
     */
    @FXML

    private void dragOver(DragEvent event) {
        /* data is dragged over the target */
        LOG.debug("onDragOver");

        // Accept it only if it is not dragged from the same control and if it
        // has a string data
        if (event.getDragboard().hasString()) {
            // allow for both copying and moving, whatever user chooses
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }

    /**
     * When the mouse is released over the FXHTMLEditor the value is written to
     * the editor.
     *
     * SceneBuilder writes the event as ActionEvent that you must change to the
     * proper event type that in this case is DragEvent
     *
     * @param event
     */
    @FXML
    private void dragDropped(DragEvent event) throws SQLException {
        LOG.debug("onDragDropped");
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            int emailId = Integer.parseInt(db.getString());
            EmailData draggedEmail = this.emailDAO.find(getDBMailConfig(), emailId);

            Text target = (Text) event.getTarget();
            LOG.debug("target: " + target.toString());
            String folderName = target.getText();
            LOG.debug("dropped on folder: " + folderName);
            int folderId = this.folderDAO.findFolderId(getDBMailConfig(), folderName);
            draggedEmail.setFolderKey(folderId);
            if (!folderName.equals("Drafts") && folderId != -1) {
                this.emailDAO.update(getDBMailConfig(), draggedEmail);
            }

            a.setAlertType(AlertType.INFORMATION);

            a.setContentText("Email Successfully moved to folder: " + folderName);
            a.show();
            LOG.debug("Email Successfully moved to folder: " + folderName);
            success = true;
            FolderData currentFolder = new FolderData();
            currentFolder.setId(folderId);
            this.emailTableController.displayEmailsFromFolder(currentFolder);
        }
        //let the source know whether the string was successfully transferred
        // and used
        event.setDropCompleted(success);

        event.consume();
    }

    /**
     * sets folder dao field
     *
     * @param folderDao
     */
    public void setFolderDAO(FolderDAO folderDao) {
        this.folderDAO = folderDao;
    }

    public void setEmailDAO(EmailDAO emailDao) {
        this.emailDAO = emailDao;
    }

    /**
     * The RootLayoutController calls this method to provide a reference to the
     * emailFXTableController from which it can request a reference to the
     * TreeView. With theTreeView reference it can change the selection in the
     * TableView.
     *
     * @param emailTableController
     */

    void setTableController(EmailTableController emailTableController) {
        this.emailTableController = emailTableController;
    }

    /**
     * displays tree
     *
     * @throws SQLException
     */
    public void displayTree() throws SQLException {
        // Retrieve the list of fish

        ObservableList<FolderData> folders = folderDAO.findAll(getDBMailConfig());

       
        if (folders != null) {
            folders.stream().map((fd) -> new TreeItem<>(fd)).map((item) -> {
                // item.setGraphic(new ImageView(getClass().getResource("/images/fish.png").toExternalForm()));
                return item;
            }).forEachOrdered((item) -> {
                folderFXTreeView.getRoot().getChildren().add(item);
            });
        }

        
        folderFXTreeView.getRoot().setExpanded(true);

        folderFXTreeView.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> showFolderContent(newValue));
    }

    /**
     * Using the reference to the emailTableController it can change the
     * selected row in the TableView It also displays the folderData object that
     * corresponds to the selected node.
     *
     * @param folderData
     */
    private void showFolderContent(TreeItem<FolderData> folderData) {

        try {
            // Select the row that contains the FishData object from the Tree
            emailTableController.displayEmailsFromFolder(folderData.getValue());
        } catch (SQLException ex) {
            LOG.error("problem displaying folder content");
            java.util.logging.Logger.getLogger(FolderTreeController.class.getName()).log(Level.SEVERE, null, ex);
        }

        LOG.info("displaying emails from folder" + folderData.getValue().getName() + "ID:" + folderData.getValue().getId());
    }

    /**
     * temporary method used to connect to the database, will be removed in
     * phase 4
     *
     * @return
     */
    private MailConfig getDBMailConfig() {
        PropertiesManager pm = new PropertiesManager();
        PropertyBean propertyBean = new PropertyBean();
        try {
            pm.loadTextProperties(propertyBean, "", "MailConfig");
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(EmailTableController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return propertyBean.getMailConfig();

    }
}
