/**
 * Sample Skeleton for 'HelpWebView.fxml' Controller Class
 */

package com.petko.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.web.WebView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebViewController {
    private final static Logger LOG = LoggerFactory.getLogger(WebViewController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    @FXML
    private WebView helpFXWebView;
            
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        final String html = "help.html";
        final java.net.URI uri = java.nio.file.Paths.get(html).toAbsolutePath().toUri();
        LOG.info("uri= " + uri.toString());

        // create WebView with specified local content
        helpFXWebView.getEngine().load(uri.toString());
    }
}
