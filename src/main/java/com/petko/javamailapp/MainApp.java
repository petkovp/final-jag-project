/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petko.javamailapp;

import com.petko.beans.PropertyBean;
import com.petko.business.PropertiesManager;
import com.petko.controller.PropertiesFormController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

/**
 * This class represents the common whaty in which a JavaFX application begins
 * that uses FXML/Controller architecture.
 *
 * @author Ken Fogel
 * @version 2.0
 */
public class MainApp extends Application {

    // slf4j log4j logger
    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    private Stage primaryStage;
    private Parent rootPane;
    private PropertyBean propertyBean;
    private PropertiesManager pm;

    private final Locale currentLocale;

    /**
     * Default constructor that instantiates the DAO object. This is done here
     * rather than in the fxml controller so that it can be shared in other
     * controllers if they existed. Optionally, being used to support changing
     * the locale to see if i18n is functioning. Methods that access the
     * resource bundles directly are overloaded to use a Locale object
     */
    public MainApp() {

        currentLocale = new Locale("en", "CA");

        log.debug("Locale = " + currentLocale);

    }

    /**
     * All JavaFX programs must override start and receive the Stage object from
     * the framework.After decorating the Stage it calls upon another method to
     * create the Scene.
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("title"));

        retrieveMailConfig();
        boolean t = true;
        if (!pm.isPropertyFilled(propertyBean)) {
            initPropertiesLayout();
            //use resources after
            this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("title"));
            Scene scene = new Scene(rootPane);
            primaryStage.setScene(scene);
            primaryStage.show();
        }
        initRootLayout();
        //use resources after
        this.primaryStage.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("title"));
        Scene scene = new Scene(rootPane);
        primaryStage.setScene(scene);
        primaryStage.show();

        log.info("Program started");
    }

    /**
     * retrieve mail config and sets property manager
     *
     * @throws IOException
     */
    private void retrieveMailConfig() throws IOException {
        pm = new PropertiesManager();
        propertyBean = new PropertyBean();
        pm.loadTextProperties(propertyBean, "", "MailConfig");

        log.debug(propertyBean.toString());
    }

    /**
     * The stop method is called before the stage is closed. You can use this
     * method to perform any actions that must be carried out before the program
     * ends. The JavaFX GUI is still running. The only action you cannot perform
     * is to cancel the Platform.exit() that led to this method.
     */
    @Override
    public void stop() {
        log.info("Stage is closing");
    }

    public void initPropertiesLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();

            loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));

            loader.setLocation(MainApp.class
                    .getResource("/fxml/PropertiesForm.fxml"));

            rootPane = (GridPane) loader.load();

            PropertiesFormController controller = loader.getController();

            controller.setupProperties(pm, propertyBean);

        } catch (IOException ex) {
            log.error("Error displaying form", ex);
            errorAlert(ex.getMessage());
        }

    }

    /**
     * Load the layout and controller for an FXML application.
     */
    public void initRootLayout() {

        try {
            // Instantiate a FXMLLoader object
            FXMLLoader loader = new FXMLLoader();

            // Configure the FXMLLoader with the i18n locale resource bundles
            loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));

            // Connect the FXMLLoader to the fxml file that is stored in the jar
            loader.setLocation(MainApp.class
                    .getResource("/fxml/MainWindow.fxml"));

            // The load command returns a reference to the root pane of the fxml file
            rootPane = (BorderPane) loader.load();
        } catch (IOException ex) {
            log.error("Error displaying form", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("errorTitle"));
        dialog.setHeaderText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("errorTitle"));
        dialog.setContentText(ResourceBundle.getBundle("MessagesBundle", currentLocale).getString("errorText"));
        dialog.show();
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {

        launch(args);

    }
}
