/*
 *Bean for attachments
 */
package com.petko.beans;

/**
 *
 * @author Petko Petkov
 * 
 */
public class AttachmentData {
    private int id;
    private String fileName;
    private byte[] picture;
    private boolean isEmbedded;
    private int emailId;
    //if isEmbedded is false, it's just a regular file

    public int getEmailId() {
        return emailId;
    }

    public void setEmailId(int emailId) {
        this.emailId = emailId;
    }

    public AttachmentData(int id, String fileName, byte[] picture, boolean isEmbedded, int emailId) {
        this.id = id;
        this.fileName = fileName;
        this.picture = picture;
        this.isEmbedded = isEmbedded;
        this.emailId = emailId;
    }
    //default constructor
public AttachmentData() {
        this.id = -1;
        this.fileName = "";
        this.picture = null;
}
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public boolean isIsEmbedded() {
        return isEmbedded;
    }

    public void setIsEmbedded(boolean isEmbedded) {
        this.isEmbedded = isEmbedded;
    }
    

}
