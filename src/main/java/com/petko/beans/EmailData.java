/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petko.beans;

import java.sql.Timestamp;
import java.util.Objects;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import jodd.mail.Email;
import jodd.mail.EmailAddress;

/**
 *
 * @author HP
 */
public class EmailData {

    private SimpleIntegerProperty id;

    private SimpleIntegerProperty folderKey;

    private Timestamp receivedDate;
    private SimpleStringProperty htmlMessage;
    private SimpleStringProperty regularMessage;
    private SimpleStringProperty from;
    private SimpleStringProperty subject;

    public Email email;

    //default constructor
    public EmailData() {
        this.id = new SimpleIntegerProperty(-1);
        this.folderKey = new SimpleIntegerProperty(0);
        this.receivedDate = null;
        this.email = null;
        this.regularMessage = new SimpleStringProperty("");
        this.htmlMessage = new SimpleStringProperty("");

    }

    public String getRegularMessage() {
        return regularMessage.get();
    }

    public void setRegularMessage(String regularMessage) {
        this.regularMessage = new SimpleStringProperty(regularMessage);
    }

    public void setSubject(String subject) {
        this.regularMessage = new SimpleStringProperty(subject);
    }

    public void setHtmlMessage(String htmlMessage) {

        this.email.htmlMessage(htmlMessage);
        this.htmlMessage = new SimpleStringProperty(htmlMessage);

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.id.get();
        hash = 59 * hash + this.folderKey.get();
        hash = 59 * hash + Objects.hashCode(this.htmlMessage.get());
        hash = 59 * hash + Objects.hashCode(this.regularMessage.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmailData other = (EmailData) obj;
        if (this.id.get() != other.id.get()) {
            return false;
        }
        if (this.folderKey.get() != other.folderKey.get()) {
            return false;
        }
        if (!Objects.equals(this.htmlMessage.get(), other.htmlMessage.get())) {
            return false;
        }
        if (!Objects.equals(this.regularMessage.get(), other.regularMessage.get())) {
            return false;
        }
        return true;
    }

    public String getHtmlMessage() {
        return htmlMessage.get();
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public int getFolderKey() {
        return folderKey.get();
    }

    public Email getEmail() {
        return this.email;
    }

    public SimpleStringProperty getFromProperty() {
        SimpleStringProperty from = new SimpleStringProperty(this.email.from().toString());
        return from;
    }

    public SimpleStringProperty getSubjectProperty() {
        String subject = "";
        if (this.email.subject() != null) {
            subject = this.email.subject().toString();
        }
        SimpleStringProperty subj = new SimpleStringProperty(subject);
        return subj;
    }

    public void setFolderKey(int folderKey) {
        this.folderKey = new SimpleIntegerProperty(folderKey);
    }

    public Timestamp getReceivedDate() {
        return receivedDate;
    }

    public SimpleStringProperty getReceivedDateProperty() {
        return new SimpleStringProperty(getReceivedDate().toString());
    }

    public void setReceivedDate(Timestamp receivedDate) {
        this.receivedDate = receivedDate;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    @Override
    public String toString() {
        StringBuilder cc = new StringBuilder(arrayToString(this.email.cc()));
        StringBuilder bcc = new StringBuilder(arrayToString(this.email.bcc()));
        StringBuilder to = new StringBuilder(arrayToString(this.email.to()));

        return "EmailData{" + "id=" + id + ", folderKey=" + folderKey + ", receivedDate=" + receivedDate + ", htmlMessage=" + htmlMessage.get() + ", regularMessage=" + regularMessage.get() + ", from=" + from + ", subject=" + subject + "cc" + cc.toString() + "bcc:" + bcc.toString() + "to" + to.toString() + ", email=" + email + '}';
    }

    /**
     * method used to simplify the access the cc recipients
     *
     * @return simplestringproperty of all to recipients
     */
    public SimpleStringProperty getCcProperty() {
        SimpleStringProperty cc = new SimpleStringProperty(arrayToString(this.email.cc()).toString());
        return cc;
    }

    /**
     * method used to simplify the access the bcc recipients
     *
     * @return simplestringproperty of all to recipients
     */
    public SimpleStringProperty getBccProperty() {
        SimpleStringProperty bcc = new SimpleStringProperty(arrayToString(this.email.bcc()).toString());
        return bcc;
    }

    /**
     * method used to simplify the access the to recipients
     *
     * @return simplestringproperty of all to recipients
     */
    public SimpleStringProperty getToProperty() {
        SimpleStringProperty to = new SimpleStringProperty(arrayToString(this.email.to()).toString());
        return to;
    }

    private StringBuilder arrayToString(EmailAddress[] array) {
        StringBuilder sbf = new StringBuilder();
        for (EmailAddress add : array) {
            sbf.append(add.getEmail());
            sbf.append(", ");
        }
        return sbf;

    }

}
