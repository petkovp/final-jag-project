/**
 * bean for email addresses 
 */
package com.petko.beans;

import java.util.Objects;

/**
 *
 * @author HP
 */
public class AddressData {
     private int id;
    private String type;
    private String address;
    private int emailId;

    public AddressData(int id, String type, String address, int emailId) {
        this.id = id;
        this.type = type;
        this.address = address;
        this.emailId = emailId;
    }
      public AddressData() {
        this.id = -1;
        this.type = "";
        this.address = "";
        this.emailId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getEmailId() {
        return emailId;
    }

    public void setEmailId(int emailId) {
        this.emailId = emailId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + this.id;
        hash = 19 * hash + Objects.hashCode(this.type);
        hash = 19 * hash + Objects.hashCode(this.address);
        hash = 19 * hash + this.emailId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AddressData other = (AddressData) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.emailId != other.emailId) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        return true;
    }
    
}
