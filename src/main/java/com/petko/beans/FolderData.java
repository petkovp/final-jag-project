package com.petko.beans;

import java.util.Objects;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * FolderData FX bean for folder that overrides toString, hashCode and equals Updated id
 * field to int and updated equals and hashCode Made all parameters final
 *
 * @author petko
 * @version 1.0
 */
public class FolderData {

    private SimpleIntegerProperty id;
    private SimpleStringProperty Name;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + this.id.get();
        hash = 11 * hash + Objects.hashCode(this.Name.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FolderData other = (FolderData) obj;
        if (this.id.get() != other.id.get()) {
            return false;
        }
        if (!Objects.equals(this.Name.get(), other.Name.get())) {
            return false;
        }
        return true;
    }

    public FolderData( final int id, final String Name) {
        this.id = new SimpleIntegerProperty(id);
        this.Name = new SimpleStringProperty(Name);
    }
    public FolderData(){
        this.id = new SimpleIntegerProperty(-1);
        this.Name= new SimpleStringProperty("");
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id = new SimpleIntegerProperty(id);
    }

    public String getName() {
        return Name.get();
    }

    public void setName(String Name) {
        this.Name = new SimpleStringProperty(Name);
    }
    
    public SimpleIntegerProperty getIdProperty(){
        return this.id;
    }
    
    public SimpleStringProperty getNameProperty(){
        return this.Name;
    }
    


    
}
