package com.petko.beans;

/**
 * Mail Config bean used to contain credentialts information to caonnect to an email
 * or a database
 * @author HP
 */
public class MailConfig {

    private String name;
    private String imapUrl;
    private String smtpUrl;
    private String imapPort;
    private String smtpPort;
    private String databaseUrl;
    private String databaseName;
    private String databasePort;
    private String databaseUsername;
    private String databasePassword;
    private String userEmailAddress;
    private String password;

    /**
     * Default Constructor
     */
    public MailConfig() {
        
        
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImapUrl(String imapUrl) {
        this.imapUrl = imapUrl;
    }

    public void setSmtpUrl(String smtpUrl) {
        this.smtpUrl = smtpUrl;
    }

    public void setImapPort(String imapPort) {
        this.imapPort = imapPort;
    }

    public void setSmtpPort(String smtpPort) {
        this.smtpPort = smtpPort;
    }

    public void setDatabaseUrl(String databaseUrl) {
        this.databaseUrl = databaseUrl;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public void setDatabasePort(String databasePort) {
        this.databasePort = databasePort;
    }

    public void setDatabaseUsername(String databaseUsername) {
        this.databaseUsername = databaseUsername;
    }

    public void setDatabasePassword(String databasePassword) {
        this.databasePassword = databasePassword;
    }
    /**
     * complete constructor
     * @param name
     * @param imapUrl
     * @param smtpUrl
     * @param imapPort
     * @param smtpPort
     * @param databaseUrl
     * @param databaseName
     * @param databasePort
     * @param databaseUsername
     * @param databasePassword
     * @param userEmailAddress
     * @param password 
     */
    public MailConfig(String name, String imapUrl, String smtpUrl, String imapPort, String smtpPort, String databaseUrl, String databaseName, String databasePort, String databaseUsername, String databasePassword, String userEmailAddress, String password) {
        this.name = name;
        this.imapUrl = imapUrl;
        this.smtpUrl = smtpUrl;
        this.imapPort = imapPort;
        this.smtpPort = smtpPort;
        this.databaseUrl = databaseUrl;
        this.databaseName = databaseName;
        this.databasePort = databasePort;
        this.databaseUsername = databaseUsername;
        this.databasePassword = databasePassword;
        this.userEmailAddress = userEmailAddress;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getImapUrl() {
        return imapUrl;
    }

    public String getSmtpUrl() {
        return smtpUrl;
    }

    public String getImapPort() {
        return imapPort;
    }

    public String getSmtpPort() {
        return smtpPort;
    }

    public String getDatabaseUrl() {
        return databaseUrl;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getDatabasePort() {
        return databasePort;
    }

    public String getDatabaseUsername() {
        return databaseUsername;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }
    
   
    /**
     * Non-default constructor
     * 
     * @param host
     * @param userEmailAddress
     * @param password
     */
    public MailConfig(final String imapUrl,final String smtpUrl, final String userEmailAddress, final String password) {
        this.imapUrl = imapUrl;
        this.smtpUrl = smtpUrl;
        this.userEmailAddress = userEmailAddress;
        this.password = password;
    }


    /**
     * @return the userEmailAddress
     */
    public final String getUserEmailAddress() {
        return userEmailAddress;
    }

    /**
     * @param userEmailAddress
     */
    public final void setUserEmailAddress(final String userEmailAddress) {
        this.userEmailAddress = userEmailAddress;
    }

    /**
     * @return the password
     */
    public final String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public final void setPassword(final String password) {
        this.password = password;
    }
    
}
