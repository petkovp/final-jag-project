/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve all the records from a user query and return them as an arraylist
 * <li>3) Close the database </ul>
 *
 * @author Petko Petkov
 * @version 1.8
 */
package com.petko.persistence;

import com.petko.beans.FolderData;
import com.petko.beans.MailConfig;
import java.sql.*;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the FolderDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the USER and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * Updated by creating a new method createFishData that used the resultSet to
 * create an object. Originally this code was repeated three times.
 *
 */
public class FolderDAOImpl implements FolderDAO {
    
    private final static Logger LOG = LoggerFactory.getLogger(FolderDAOImpl.class);

    // This information should be coming from a Properties file
//    private final static String URL = "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
//    private final static String USER = "mailappuser";
//    private final static String PASSWORD = "petkostandard";
    /**
     * Retrieve all the records for the given table and returns the data as an
     * arraylist this can be used to diplay all the folders of auser
     *
     * @return The arraylist of FishData objects
     * @throws java.sql.SQLException
     */
    @Override
    public ObservableList<FolderData> findAll(MailConfig mc) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        ObservableList<FolderData> rows = FXCollections
                .observableArrayList();
        
        String selectQuery = "SELECT ID, FOLDERNAME FROM FOLDER";
        
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement pStatement = connection.prepareStatement(selectQuery);  ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                rows.add(createFolderData(resultSet));
            }
        }
        LOG.info("total number of folders found : " + rows.size());
        return rows;
    }

    /**
     * finds the id of a folder by using its name 
     *
     * @param mc
     * @param name
     * @return The arraylist of FishData objects
     * @throws java.sql.SQLException
     */
    @Override
    public int findFolderId(MailConfig mc, String name) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        LOG.debug("finding id of folder:" +name);
        
        
        String selectQuery = "SELECT ID FROM FOLDER where FOLDERNAME=?";
        
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                ) {
            PreparedStatement pStatement = connection.prepareStatement(selectQuery);
            pStatement.setString(1, name);
            ResultSet resultSet = pStatement.executeQuery();
            if (resultSet.next()) {
                
                int folderId = resultSet.getInt("ID");
                LOG.info(name+" folder was found id: "+folderId);
                return folderId;
                
            } else {
                LOG.error(name+" folder was not  found id: ");
                return -1;
            }
        }
       
    }

    /**
     * Private method that creates an object of type FolderData from the current
     * record in the ResultSet
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    private FolderData createFolderData(ResultSet resultSet) throws SQLException {
        FolderData folderData = new FolderData();
        folderData.setId(resultSet.getInt("ID"));
        folderData.setName(resultSet.getString("FOLDERNAME"));
        
        return folderData;
    }

    /**
     * This method adds a FolderData object as a record to the database. The
     * column list does not include ID as this is an auto increment value in the
     * table.
     *
     * @param folderData
     * @return The number of records created, should always be 1
     * @throws SQLException
     */
    @Override
    public int create(MailConfig mc, FolderData folderData) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;
        String createQuery = "INSERT INTO FOLDER (FOLDERNAME) VALUES (?)";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                  PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            fillPreparedStatementForFolder(ps, folderData);
            result = ps.executeUpdate();

            // Retrieve generated primary key value and assign to bean
            try ( ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                folderData.setId(recordNum);
                LOG.debug("New record ID is " + recordNum);
            }
        }
        LOG.info("# of records created : " + result);
        return result;
    }

    /**
     * Add the fields from a FolderData object to the PreparedString on behalf
     * of the create/insert method
     *
     * @param ps
     * @param fishData
     * @throws SQLException
     */
    private void fillPreparedStatementForFolder(PreparedStatement ps, FolderData folderData) throws SQLException {
        ps.setString(1, folderData.getName());
    }

    /**
     * This method deletes a single record based on the criteria of the primary
     * key field ID value. It should return either 0 meaning that there is no
     * record with that ID or 1 meaning a single record was deleted. If the
     * value is greater than 1 then something unexpected has happened. A
     * criteria other than ID may delete more than one record.
     *
     * @param id The primary key to use to identify the record that must be
     * deleted
     * @return The number of records deleted, should be 0 or 1
     * @throws SQLException
     */
    @Override
    public int delete(MailConfig mc, int id) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;
        
        String deleteQuery = "DELETE FROM FOLDER WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        LOG.info("# of records deleted : " + result);
        return result;
    }

    /**
     * This method will update all the fields of a record except ID. Usually
     * updates are tied to specific fields and so only those fields need appear
     * in the SQL statement.
     *
     * @param folderData An object with an existing ID and new data in the
     * fields
     * @return The number of records updated, should be 0 or 1
     * @throws SQLException
     *
     */
    @Override
    public int update(MailConfig mc, FolderData folderData) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;
        
        String updateQuery = "UPDATE FOLDER SET FOLDERNAME=? WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                  PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, folderData.getName());
            ps.setInt(2, folderData.getId());
            
            result = ps.executeUpdate();
        }
        LOG.info("# of records updated : " + result);
        return result;
    }

    private boolean validateMailConfig(MailConfig mc) throws SQLException {
        boolean reachable;
        try {
            Connection conn = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword());
            reachable = conn.isValid(3);
        } catch (SQLException e) {
            return false;
        }
        return reachable;
        
    }
}
