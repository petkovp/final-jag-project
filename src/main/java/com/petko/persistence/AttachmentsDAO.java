package com.petko.persistence;


import com.petko.beans.AttachmentData;
import com.petko.beans.EmailData;
import com.petko.beans.MailConfig;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for CRUD methods related to attachments
 *
 * @author Petko
 */
public interface AttachmentsDAO {

    // Creates and links an address to an email record
    public int create( MailConfig mc,AttachmentData ad) throws SQLException;

    
    // Read find attachments linked to an email by getting email id
    public List<AttachmentData> findAttachmentsForEmail( MailConfig mc,int emailId)throws SQLException;
   
    public int saveAttachmentsForEmail(MailConfig mc, EmailData emailData) throws SQLException;
    // in a mail application, you have to delete the file in order to modify it
    //public int update(AttachmentData ad) throws SQLException;
    public int addAttachmentsToEmail(MailConfig mc, EmailData emailData) throws SQLException;
    // Delete
    public int delete( MailConfig mc,AttachmentData ad) throws SQLException;
}
