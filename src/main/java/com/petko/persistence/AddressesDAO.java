package com.petko.persistence;


import com.petko.beans.AddressData;
import com.petko.beans.EmailData;
import com.petko.beans.MailConfig;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for CRUD methods related to addresses
 *
 * @author Petko
 */
public interface AddressesDAO {

    // Creates and links an address to an email record
    public int create(MailConfig mc,AddressData ad) throws SQLException;
    public EmailData createAddressesForEmail(MailConfig mc, EmailData emailData) throws SQLException;
   
    // Read
    public List<AddressData> findAll(MailConfig mc) throws SQLException;
    public void findAdressesForEmail(MailConfig mc, EmailData email) throws SQLException;
   // Update
    public int update(MailConfig mc,AddressData ad) throws SQLException;

    // Delete
    public int delete(MailConfig mc,AddressData addressData) throws SQLException;
}
