/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve all the records from a user query and return them as an arraylist
 * <li>3) Close the database </ul>
 * Added logging Changed read to 3 methods, findAll, findID and findDiet
 * Eliminated returning null references
 *
 * @author Ken Fogel
 * @version 1.8
 */
package com.petko.persistence;

import com.petko.beans.AddressData;
import com.petko.beans.EmailData;
import com.petko.beans.MailConfig;
import java.sql.*;
import java.util.*;
import javax.naming.AuthenticationException;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the AddressesDAO interface
 *
 *
 */
public class AddressesDAOImpl implements AddressesDAO {

    private final static Logger LOG = LoggerFactory.getLogger(AddressesDAOImpl.class);

    /**
     * creates an address record and calls linkEmailToAddress to link the
     * address to an email in the bridge table
     *
     * @param mc
     * @param addressData
     * @param type
     * @return
     * @throws SQLException
     */
    @Override
    public int create(MailConfig mc, AddressData addressData) throws SQLException {

        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;
        String createQuery = "INSERT INTO ADDRESSES (ADDRESS) VALUES (?)";

        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // Using a prepared statement to handle the conversion
                  PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, addressData.getAddress());
            result = ps.executeUpdate();

            try ( ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                addressData.setId(recordNum);
                //adds record to bridge table
                linkEmailToAddress(mc, addressData.getEmailId(), addressData.getId(), addressData.getType());
                LOG.debug("New record ID is " + recordNum);

            }
        }
        LOG.info("# of records created : " + result);
        return result;

    }

    /**
     * creates an address record and calls linkEmailToAddress to link the
     * address to an email in the bridge table
     *
     * @param mc
     * @param emailData
     * @return
     * @throws SQLException
     */
    @Override
    public EmailData createAddressesForEmail(MailConfig mc, EmailData emailData) throws SQLException {

        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        Email email = emailData.getEmail();

        for (EmailAddress address : email.cc()) {
            LOG.debug("first cc address: " + address);
            AddressData ad = createAddressData("cc", address.getEmail(), emailData.getId());
            create(mc, ad);
        }
        for (EmailAddress address : email.to()) {
            AddressData ad = createAddressData("to", address.getEmail(), emailData.getId());
            create(mc, ad);
        }
        for (EmailAddress address : email.bcc()) {
            AddressData ad = createAddressData("bcc", address.getEmail(), emailData.getId());
            create(mc, ad);
        }
        return emailData;

    }

    /**
     * creates and returns and address data
     *
     * @param type
     * @param address
     * @param emailId
     * @return
     */
    private AddressData createAddressData(String type, String address, int emailId) {

        AddressData addressData = new AddressData();
        addressData.setAddress(address);
        addressData.setEmailId(emailId);
        addressData.setType(type);
        return addressData;
    }

    /**
     * creates a record in the bridge table to link an address to an email
     * returns 1 if record was cerated 0 if there was an error
     *
     * @param emailId
     * @param addressId
     * @param type
     * @throws SQLException
     */
    private int linkEmailToAddress(MailConfig mc, int emailId, int addressId, String type) throws SQLException {
        String createQuery = "INSERT INTO EMAILBRADDRESSES (TYPE,EMAILID,ADDRESSID) VALUES (?,?,?)";
        int result;
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                  PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, type);
            ps.setInt(2, emailId);
            ps.setInt(3, addressId);

            result = ps.executeUpdate();

        }
        return result;
    }

    /**
     * Retrieve all the addresses this can be used in order to display all the
     * contacts of a user
     *
     * @param mc
     * @return The arraylist of AddressData objects
     * @throws java.sql.SQLException
     */
    @Override
    public List<AddressData> findAll(MailConfig mc) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        List<AddressData> rows = new ArrayList<>();

        String selectQuery = "SELECT ADDRESS,ADDRESSID,EMAILID,TYPE FROM ADDRESSES "
                + "JOIN EMAILBRADDRESSES ON ADDRESSES.ID=ADDRESSID "
                + "JOIN EMAIL ON EMAIL.ID=EMAILID ";

        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement pStatement = connection.prepareStatement(selectQuery);  ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                rows.add(createAddressData(resultSet));
            }
        }
        LOG.info("# of records found : " + rows.size());
        return rows;
    }

    /**
     * Retrieve all the addresses for an emailData and add the to the email
     * property
     *
     * @param mc
     * @return The arraylist of AddressData objects
     * @throws java.sql.SQLException
     */
    @Override
    public void findAdressesForEmail(MailConfig mc, EmailData email) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        findAllCc(mc, email);
        findAllBcc(mc, email);
        findAllTo(mc, email);

    }

    /**
     * Retrieve all the cc addresses for an EmailData object and add the to the
     * email property contacts of a user
     *
     * @param mc
     * @param email
     * @return The arraylist of AddressData objects
     * @throws java.sql.SQLException
     */
    private List<String> findAllCc(MailConfig mc, EmailData email) throws SQLException {

        ArrayList<String> rows = new ArrayList<>();
        String selectQuery = "SELECT ADDRESS,ADDRESSID,EMAILID,TYPE FROM ADDRESSES "
                + "JOIN EMAILBRADDRESSES ON ADDRESSES.ID=ADDRESSID "
                + "JOIN EMAIL ON EMAIL.ID=EMAILID WHERE EMAIL.ID=? AND TYPE=? ";

        // Using try with resources
        // This ensures that the objects in the parenthesis () will be closed
        // when block ends. In this case the Connection, PreparedStatement and
        // the ResultSet will all be closed.
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                ) {
            PreparedStatement pStatement = connection.prepareStatement(selectQuery);
            pStatement.setInt(1, email.getId());
            pStatement.setString(2, "cc");
            ResultSet resultSet = pStatement.executeQuery();
            while (resultSet.next()) {
                rows.add(resultSet.getString("ADDRESS"));
            }
        }
        handleCc(rows, email.getEmail());
        LOG.info("# of cc records found : " + rows.size());
        return rows;
    }

    /**
     * Retrieve all the bcc addresses for an EmailData object and add the to the
     * email property contacts of a user
     *
     * @param mc
     * @param email
     * @return The arraylist of AddressData objects
     * @throws java.sql.SQLException
     */
    private List<String> findAllBcc(MailConfig mc, EmailData email) throws SQLException {

        ArrayList<String> rows = new ArrayList<>();
        String selectQuery = "SELECT ADDRESS,ADDRESSID,EMAILID,TYPE FROM ADDRESSES "
                + "JOIN EMAILBRADDRESSES ON ADDRESSES.ID=ADDRESSID "
                + "JOIN EMAIL ON EMAIL.ID=EMAILID WHERE EMAIL.ID=? AND TYPE=? ";

        // Using try with resources
        // This ensures that the objects in the parenthesis () will be closed
        // when block ends. In this case the Connection, PreparedStatement and
        // the ResultSet will all be closed.
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                ) {
            PreparedStatement pStatement = connection.prepareStatement(selectQuery);
            pStatement.setInt(1, email.getId());
            pStatement.setString(2, "bcc");
            ResultSet resultSet = pStatement.executeQuery();
            while (resultSet.next()) {
                rows.add(resultSet.getString("ADDRESS"));
            }
        }
        handleBcc(rows, email.getEmail());
        LOG.info("# of bcc records found : " + rows.size());
        return rows;
    }

    /**
     * Retrieve all the To addresses for an EmailData object and add the to the
     * email property contacts of a user
     *
     * @param mc
     * @param email
     * @return The arraylist of AddressData objects
     * @throws java.sql.SQLException
     */
    private List<String> findAllTo(MailConfig mc, EmailData email) throws SQLException {

        ArrayList<String> rows = new ArrayList<>();
        String selectQuery = "SELECT ADDRESS,ADDRESSID,EMAILID,TYPE FROM ADDRESSES "
                + "JOIN EMAILBRADDRESSES ON ADDRESSES.ID=ADDRESSID "
                + "JOIN EMAIL ON EMAIL.ID=EMAILID WHERE EMAIL.ID=? AND TYPE=? ";

        // Using try with resources
        // This ensures that the objects in the parenthesis () will be closed
        // when block ends. In this case the Connection, PreparedStatement and
        // the ResultSet will all be closed.
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                ) {
            PreparedStatement pStatement = connection.prepareStatement(selectQuery);
            pStatement.setInt(1, email.getId());
            pStatement.setString(2, "to");
            ResultSet resultSet = pStatement.executeQuery();
            while (resultSet.next()) {
                rows.add(resultSet.getString("ADDRESS"));
            }
        }
        handleTo(rows, email.getEmail());
        LOG.info("# of to records found : " + rows.size());
        return rows;
    }

    /**
     * handles ArrayList of email addresses and adds them as "to" to an email
     * object
     *
     * @param to
     * @param mail
     * @return true if added at least one "to", false if nothing is added
     */
    private boolean handleTo(ArrayList<String> to, Email mail) {
        boolean toHandled = false;
        if (to.size() == 0) {
            toHandled = true;
        }
        for (String emailAddress : to) {

            mail.to(emailAddress);
            toHandled = true;

        }
        return toHandled;
    }

    /**
     * handles ArrayList of email addresses and adds them as "cc" to an email
     * object
     *
     * @param cc
     * @param mail
     * @return true if added at least one "cc", false if nothing is added
     */
    private boolean handleCc(ArrayList<String> cc, Email mail) {
        boolean ccHandled = false;
        if (cc.size() == 0) {
            ccHandled = true;
        }
        for (String emailAddress : cc) {

            mail.cc(emailAddress);
            ccHandled = true;

        }
        return ccHandled;
    }

    /**
     * handles ArrayList of email addresses and adds them as "bcc" to an email
     * object
     *
     * @param bcc
     * @param mail
     * @return true if added at least one "bcc", false if nothing is added
     */
    private boolean handleBcc(ArrayList<String> bcc, Email mail) {
        boolean bccHandled = false;
        if (bcc.size() == 0) {
            bccHandled = true;
        }
        for (String emailAddress : bcc) {

            mail.bcc(emailAddress);
            bccHandled = true;

        }
        return bccHandled;
    }

    /**
     * takes a resultset and transforms it into an addressData object
     *
     * @param r
     * @return
     * @throws SQLException
     */
    private AddressData createAddressData(ResultSet r) throws SQLException {
        AddressData ad = new AddressData();
        ad.setAddress(r.getString("ADDRESS"));
        ad.setId(r.getInt("ADDRESSID"));
        ad.setEmailId(r.getInt("EMAILID"));
        ad.setType(r.getString("TYPE"));
        return ad;
    }

    /**
     * This method deletes a single record based on the criteria of the primary
     * key field ID value.It should return either 0 meaning that there is no
     * Record with that ID or 1 meaning a single record was deleted.If the value
     * is greater than 1 then something unexpected has happened. A criteria
     * other than ID may delete more than one record.
     *
     * @param mc
     * @param addressData that is being deleted
     * @return The number of records deleted, should be 0 or 1
     * @throws SQLException
     */
    @Override
    public int delete(MailConfig mc, AddressData addressData) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;

        String deleteQuery = "DELETE FROM ADDRESSES WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            //checks if a record was deleted in the bridgetable before deleting from addresses
            if (deleteBridgeTableRecord(mc, addressData) > 0) {
                ps.setInt(1, addressData.getId());
                result = ps.executeUpdate();
            } else {
                result = 0;
            }
        }
        LOG.info("# of records deleted : " + result);
        return result;
    }

    /**
     * method that is used to delete the bridge table record related to an
     * addressData object that is being deleted
     *
     * @param addressData
     * @return
     * @throws SQLException
     */
    private int deleteBridgeTableRecord(MailConfig mc, AddressData addressData) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;

        String deleteQuery = "DELETE FROM EMAILBRADDRESSES WHERE ADDRESSID=? AND EMAILID =?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, addressData.getId());
            ps.setInt(2, addressData.getEmailId());
            result = ps.executeUpdate();
        }
        LOG.info("# of records deleted : " + result);
        return result;
    }

    /**
     * This method will update all the fields of a record except ID.Usually
     * updates are tied to specific fields and so only those fields need appear
     * in the SQL statement.
     *
     * @param mc
     * @param addressData
     * @return The number of records updated, should be 0 or 1
     * @throws SQLException
     *
     */
    @Override
    public int update(MailConfig mc, AddressData addressData) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;
        //UPDATE EMAIL SET   DATE='2007-01-18 17:26:48', SUBJECT='HI', MESSAGE='HELLO',HTML='<HTML>',FROMWHO='ME@GMAIL.COM' WHERE ID = 1;

        String updateQuery = "UPDATE ADDRESSES SET   ADDRESS=? WHERE ID =?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                  PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, addressData.getAddress());
            ps.setInt(2, addressData.getId());

            result = ps.executeUpdate();
            if (!updateAddressBridgeTable(mc, addressData)) {
                throw new SQLException("failed to update address bridge table");
            }
        }
        LOG.info("# of records updated : " + result);
        return result;
    }

    /**
     * when an address is updated, when passed to this method, the bridge table
     * is updated
     *
     * @param ad
     * @return
     * @throws SQLException
     */
    private boolean updateAddressBridgeTable(MailConfig mc, AddressData ad) throws SQLException {
        String updateQuery = "UPDATE EMAILBRADDRESSES SET  TYPE=? WHERE ADDRESSID = ? AND EMAILID=?";

        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                  PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, ad.getType());
            ps.setInt(2, ad.getId());
            ps.setInt(1, ad.getEmailId());

            if (ps.executeUpdate() == 1) {
                return true;
            } else {
                return false;
            }
        }

    }

    /**
     * Method used to validate a connection to the database
     *
     * @param mc
     * @return
     * @throws SQLException
     */
    private boolean validateMailConfig(MailConfig mc) throws SQLException {
        boolean reachable;
        try {
            Connection conn = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword());
            reachable = conn.isValid(3);
        } catch (SQLException e) {
            return false;
        }
        return reachable;

    }

}
