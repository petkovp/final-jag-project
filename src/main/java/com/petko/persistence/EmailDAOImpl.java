/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve all the records from a user query and return them as an arraylist
 * <li>3) Close the database </ul>
 * Added logging Changed read to 3 methods, findAll, findID and findDiet
 * Eliminated returning null references
 *
 * @author Ken Fogel
 * @version 1.8
 */
package com.petko.persistence;

import com.petko.beans.EmailData;
import com.petko.beans.FolderData;
import com.petko.beans.MailConfig;
import java.sql.*;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jodd.mail.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the FishDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the USER and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * Updated by creating a new method createFishData that used the resultSet to
 * create an object. Originally this code was repeated three times.
 *
 */
public class EmailDAOImpl implements EmailDAO {

    private final static Logger LOG = LoggerFactory.getLogger(EmailDAOImpl.class);

    // This information should be coming from a Properties file
//    private final static String URL = "jdbc:mysql://localhost:3306/AQUARIUM?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
//    private final static String USER = "fish";
//    private final static String PASSWORD = "kfstandard";
    /**
     * Retrieve all the records for the given table and returns the data as an
     * arraylist of EmailData objects
     *
     * @return The arraylist of EmailData objects
     * @throws java.sql.SQLException
     */
    @Override
    public ObservableList<EmailData> findAll(MailConfig mc) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        ObservableList<EmailData> rows = FXCollections
                .observableArrayList();

        String selectQuery = "SELECT ID,FOLDERKEY,DATE,SUBJECT,MESSAGE,HTML,FROMWHO  FROM EMAIL";

        // Using try with resources
        // This ensures that the objects in the parenthesis () will be closed
        // when block ends. In this case the Connection, PreparedStatement and
        // the ResultSet will all be closed.
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement pStatement = connection.prepareStatement(selectQuery);  ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                rows.add(createEmailData(resultSet));
            }
        }
        LOG.info("# of records found : " + rows.size());
        return rows;
    }

    /**
     * Retrieve all the records for the given table and returns the data as an
     * arraylist of EmailData objects
     *
     * @return The arraylist of EmailData objects
     * @throws java.sql.SQLException
     */
    @Override
    public ObservableList<EmailData> findAllFromFolder(MailConfig mc, FolderData folderData) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        ObservableList<EmailData> rows = FXCollections
                .observableArrayList();
        int folderId = folderData.getId();
        LOG.debug("Folder ID :" + folderId);

        String selectQuery = "SELECT ID,FOLDERKEY,DATE,SUBJECT,MESSAGE,HTML,FROMWHO  FROM EMAIL where FOLDERKEY=?";

        // Using try with resources
        // This ensures that the objects in the parenthesis () will be closed
        // when block ends. In this case the Connection, PreparedStatement and
        // the ResultSet will all be closed.
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); ) {
            PreparedStatement pStatement = connection.prepareStatement(selectQuery);
            pStatement.setInt(1, folderId);
            ResultSet resultSet = pStatement.executeQuery();

            while (resultSet.next()) {
                rows.add(createEmailData(resultSet));
            }
        }
        LOG.info("# of records found : " + rows.size() + "IN FOLDER" + folderData.getName());
        return rows;
    }

    /**
     * Retrieves all the emails containing a string
     *
     * @param string
     * @return The FishData object
     * @throws java.sql.SQLException
     */
    @Override
    public List<EmailData> findAllContaining(MailConfig mc, String string) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        List<EmailData> emailData = new ArrayList<>();
        String wildChar = "'%" + string + "%'";
        String selectQuery = "SELECT ID,FOLDERKEY,DATE,SUBJECT,MESSAGE,HTML,FROMWHO  FROM EMAIL WHERE SUBJECT LIKE ? OR MESSAGE LIKE ? OR HTML LIKE ?";
        // Using try with resources
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            // pstmt.setString(1, notes + "%"); might cause an error
            pStatement.setString(1, "%" + string + "%");
            pStatement.setString(2, "%" + string + "%");
            pStatement.setString(3, "%" + string + "%");
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try ( ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    emailData.add(createEmailData(resultSet));
                }
            }
        }
        LOG.info("Found " + emailData.size() + " emails containing:" + string);
        return emailData;
    }

    /**
     * Private method that creates an object of type EmailData from the current
     * record in the ResultSet
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    private EmailData createEmailData(ResultSet resultSet) throws SQLException {

        EmailData emailData = new EmailData();
        // ID,DATE,SUBJECT,MESSAGE,HTML,FROMWHO  
        emailData.setId(resultSet.getInt("ID"));
        emailData.setReceivedDate(resultSet.getTimestamp("DATE"));//may have to change 
        emailData.setEmail(createEmailObject(resultSet.getString("FROMWHO"), resultSet.getString("SUBJECT"), resultSet.getString("MESSAGE"), resultSet.getString("HTML")));
        emailData.setRegularMessage(resultSet.getString("MESSAGE"));
        emailData.setHtmlMessage(resultSet.getString("HTML"));
        emailData.setFolderKey(resultSet.getInt("FOLDERKEY"));

        return emailData;
    }

    private Email createEmailObject(String from, String subj, String msg, String html) {

        Email email = new Email();
        email.from(from);
        email.subject(subj);
        email.textMessage(msg);
        email.htmlMessage(html);

        return email;

    }

    /**
     * This method adds a EmailData object as a record to the database. The
     * column list does not include ID as this is an auto increment value in the
     * table.
     *
     * @param emailData
     * @return The number of records created, should always be 1
     * @throws SQLException
     */
    @Override
    public int create(MailConfig mc, EmailData emailData) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        LOG.debug("emaildata in emaildaoimpl : "+emailData.toString());
        int result;
        String createQuery = "INSERT INTO EMAIL (DATE,SUBJECT,MESSAGE,HTML,FROMWHO,FOLDERKEY) VALUES (?,?,?,?,?,?)";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
            PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            fillPreparedStatementForEmail(mc, ps, emailData);
            result = ps.executeUpdate();

            // Retrieve generated primary key value and assign to bean
            try ( ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                emailData.setId(recordNum);
                LOG.debug(emailData.toString());
                AddressesDAO ad = new AddressesDAOImpl();
                ad.createAddressesForEmail(mc, emailData);
               
                AttachmentsDAO attachmentsManager = new AttachmentsDAOImpl();
                attachmentsManager.saveAttachmentsForEmail(mc, emailData);
                
                LOG.debug("New record ID is " + recordNum);

            }
        }
        LOG.info("# of records created : " + result);
        return result;
    }

    /**
     * Add the fields from a FishData object to the PreparedString on behalf of
     * the create/insert method
     *
     * @param MailConfig
     * @param ps
     * @param emailData
     * @throws SQLException
     */
    private void fillPreparedStatementForEmail(MailConfig mc, PreparedStatement ps, EmailData emailData) throws SQLException {
        //"INSERT INTO EMAIL (DATE,SUBJECT,MESSAGE,HTML,FROMWHO)
        // INSERT INTO EMAIL (DATE,SUBJECT,MESSAGE,HTML,FROMWHO) VALUES (?,?,?,?,?)
        LOG.debug("prepared Statement for an inserting email"+"Subj:  "+emailData.email.subject()+
                "regMsg: "+emailData.getRegularMessage()+
                "htmlmsg : "+emailData.getHtmlMessage());
        ps.setTimestamp(1, emailData.getReceivedDate());
        ps.setString(2, emailData.email.subject());
        ps.setString(3, emailData.getRegularMessage());
        ps.setString(4, emailData.getHtmlMessage());
        ps.setString(5, emailData.email.from().toString());
        ps.setInt(6, emailData.getFolderKey());
        LOG.info("Prepared statement created successfully");
    }

    /**
     * This method deletes a single record based on the criteria of the primary
     * key field ID value.It should return either 0 meaning that there is no
 record with that ID or 1 meaning a single record was deleted. If the
 value is greater than 1 then something unexpected has happened. A
 criteria other than ID may delete more than one record.
     *
     * @param mc
     * @param id The primary key to use to identify the record that must be
     * deleted
     * @return The number of records deleted, should be 0 or 1
     * @throws SQLException
     */
    @Override
    public int delete(MailConfig mc, int id) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;

        String deleteQuery = "DELETE FROM EMAIL WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        LOG.info("# of records deleted : " + result);
        return result;
    }

    /**
     * This method will update all the fields of a record except ID. Usually
     * updates are tied to specific fields and so only those fields need appear
     * in the SQL statement.
     *
     * @param mc maildConfig
     * @param emailData An object with an existing ID and new data in the fields
     * @return The number of records updated, should be 0 or 1
     * @throws SQLException
     *
     */
    @Override
    public int update(MailConfig mc, EmailData emailData) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        int result;
        //UPDATE EMAIL SET   DATE='2007-01-18 17:26:48', SUBJECT='HI', MESSAGE='HELLO',HTML='<HTML>',FROMWHO='ME@GMAIL.COM' WHERE ID = 1;

        String updateQuery = "UPDATE EMAIL SET   DATE=?, SUBJECT=?, MESSAGE=?,HTML=?,FROMWHO=?, FOLDERKEY=? WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                  PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setTimestamp(1, emailData.getReceivedDate());
            ps.setString(2, emailData.email.subject());
            ps.setString(3, emailData.getRegularMessage());
            ps.setString(4, emailData.getHtmlMessage());
            ps.setString(5, emailData.email.from().toString());
            ps.setInt(6, emailData.getFolderKey());
            ps.setInt(7, emailData.getId());
            result = ps.executeUpdate();
        }
        LOG.info("# of records updated : " + result);
        return result;
    }

    @Override
    public EmailData find(MailConfig mc, int id) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        EmailData found;

        String selectQuery = "SELECT FOLDERKEY,ID,DATE,SUBJECT,MESSAGE,HTML,FROMWHO  FROM EMAIL WHERE ID=?";

        // Using try with resources
        // This ensures that the objects in the parenthesis () will be closed
        // when block ends. In this case the Connection, PreparedStatement and
        // the ResultSet will all be closed.
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            pStatement.setInt(1, id);
            ResultSet resultSet = pStatement.executeQuery();

            if (resultSet.next()) {
                found = createEmailData(resultSet);
                
                //finds addresses
                AddressesDAO add = new AddressesDAOImpl();
                add.findAdressesForEmail(mc, found);
            } else {

                found = null;
            }
        }
        LOG.info("email was found");
        return found;
    }

    /**
     * checks if the mailconfig can connect to the database
     *
     * @param mc
     * @return
     * @throws SQLException
     */
    private boolean validateMailConfig(MailConfig mc) throws SQLException {
        boolean reachable;
        try {
            Connection conn = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword());
            reachable = conn.isValid(3);
        } catch (SQLException e) {
            return false;
        }
        return reachable;

    }

}
