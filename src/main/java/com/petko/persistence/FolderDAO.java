package com.petko.persistence;


import com.petko.beans.FolderData;
import com.petko.beans.MailConfig;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.ObservableList;

/**
 * Interface for CRUD methods
 *
 * @author petko
 */
public interface FolderDAO {

    // Create
    public int create( MailConfig mc,FolderData folderData) throws SQLException;

    // Read
    public ObservableList<FolderData> findAll( MailConfig mc) throws SQLException;
    
    // Read
    public int findFolderId( MailConfig mc,String folderName) throws SQLException;

    
    // Update
    public int update( MailConfig mc,FolderData folderData) throws SQLException;

    // Delete
    public int delete( MailConfig mc,int ID) throws SQLException;
}
