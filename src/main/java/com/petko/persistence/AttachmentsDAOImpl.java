/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve all the records from a user query and return them as an arraylist
 * <li>3) Close the database </ul>
 * Added logging Changed read to 3 methods, findAll, findID and findDiet
 * Eliminated returning null references
 *
 * @author Ken Fogel
 * @version 1.8
 */
package com.petko.persistence;

import com.petko.beans.AttachmentData;
import com.petko.beans.EmailData;
import com.petko.beans.MailConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import javax.activation.DataSource;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
import org.apache.logging.log4j.core.util.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the FishDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the USER and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * Updated by creating a new method createFishData that used the resultSet to
 * create an object. Originally this code was repeated three times.
 *
 */
public class AttachmentsDAOImpl implements AttachmentsDAO {

    private final static Logger LOG = LoggerFactory.getLogger(AttachmentsDAOImpl.class);

//    // This information should be coming from a Properties file
//    private final static String URL = "jdbc:mysql://localhost:3306/AQUARIUM?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
//    private final static String USER = "fish";
//    private final static String PASSWORD = "kfstandard";
    /**
     * creates an address record and calls linkAttachmentToemail
     *
     * @param attachmentData
     * @return
     * @throws SQLException
     */
    @Override
    public int create(MailConfig mc, AttachmentData attachmentData) throws SQLException {
        int result;
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        String createQuery = "INSERT INTO BINARYFILES (FILENAME, BINARYDATA) VALUES (?,?)";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                  PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, attachmentData.getFileName());
            ps.setBytes(2, attachmentData.getPicture());
            result = ps.executeUpdate();

            // Retrieve generated primary key value and assign to bean
            try ( ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                attachmentData.setId(recordNum);
                //adds record to bridge table
                linkAttachmentToemail(mc, attachmentData);
                LOG.debug("New record ID is " + recordNum);

            }
        }
        LOG.info("# File records created : " + result);
        return result;

    }

    /**
     * saves to the database all the attachments of an email
     *
     * @param mc
     * @param emailData
     * @return number of saved attachments
     */
    public int saveAttachmentsForEmail(MailConfig mc, EmailData emailData) throws SQLException {
        Email emailObject = emailData.getEmail();
        int emailId = emailData.getId();
        List<EmailAttachment<? extends DataSource>> attachments = emailObject.attachments();

        int saved = 0;
        for (EmailAttachment ea : attachments) {

            String fileName = ea.getName();
            byte[] picture = ea.toByteArray();
            AttachmentData ad = createAttachmentData(emailId, fileName, ea.isEmbedded(), picture);
            create(mc, ad);
            saved++;
            LOG.info("Saved file to database"+fileName );
        }
        LOG.info("# of saved attachmenents to db" + saved);
        return saved;
    }

    /**
     * links the AttachmentData object to the email through the bridge table
     *
     * @param ad
     * @throws SQLException
     */
    private void linkAttachmentToemail(MailConfig mc, AttachmentData ad) throws SQLException {
        String createQuery = "INSERT INTO EMAILBRBINARYFILES (TYPE,EMAILID,FILEID) VALUES (?,?,?);";
        int result;
        String fileType;
        if (ad.isIsEmbedded()) {
            fileType = "embedded";
        } else {
            fileType = "regular";
        }
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // Using a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                  PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            ps.setString(1, fileType);
            ps.setInt(2, ad.getEmailId());
            ps.setInt(3, ad.getId());

            result = ps.executeUpdate();

        }

    }

    /**
     * takes an emailData Object, retrieves the related attachments and add them
     * to the email object
     *
     * @param mc
     * @param emailData
     * @return
     */
    public int addAttachmentsToEmail(MailConfig mc, EmailData emailData) throws SQLException {
        List<AttachmentData> attachmentsFound = findAttachmentsForEmail(mc, emailData.getId());

        Email emailObject = emailData.getEmail();
        int count=0;
        for (AttachmentData attachmentData : attachmentsFound) {
            try ( FileOutputStream fos = new FileOutputStream(attachmentData.getFileName())) {
                fos.write(attachmentData.getPicture());
                if (attachmentData.isIsEmbedded()) {
                    emailObject.embeddedAttachment(EmailAttachment.with().content(attachmentData.getFileName()));
                } else {
                    emailObject.attachment(EmailAttachment.with().content(attachmentData.getFileName()));
                }
                count++;
                LOG.info("Attachment added to email object:  "+attachmentData.getFileName() );
            } catch (FileNotFoundException ex) {
                java.util.logging.Logger.getLogger(AttachmentsDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(AttachmentsDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return count;
    }

    /**
     * Retrieve all the addresses for the given table and returns the data as an
     * arraylist of strings
     *
     * @return The arraylist of EmailData objects
     * @throws java.sql.SQLException
     */
    @Override
    public List<AttachmentData> findAttachmentsForEmail(MailConfig mc, int emailId) throws SQLException {
        if (!validateMailConfig(mc)) {
            throw new SQLClientInfoException();
        }
        List<AttachmentData> rows = new ArrayList<>();

        String selectQuery = "SELECT FILENAME,BINARYDATA,FILEID,EMAILID,TYPE FROM BINARYFILES "
                + "JOIN EMAILBRBINARYFILES ON BINARYFILES.ID=FILEID "
                + "JOIN EMAIL ON EMAIL.ID=EMAILID WHERE EMAILID=?";

       try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword())) {
            PreparedStatement pStatement = connection.prepareStatement(selectQuery);
            pStatement.setInt(1, emailId);
            ResultSet resultSet = pStatement.executeQuery();

            while (resultSet.next()) {
                rows.add(createAttachmentData(resultSet));
            }
        }
        LOG.info("# of records found : " + rows.size());
        return rows;
    }

    /**
     * takes a resultset and transforms it into an attachment data object
     *
     * @param r
     * @return
     * @throws SQLException
     */
    private AttachmentData createAttachmentData(ResultSet r) throws SQLException {
        AttachmentData ad = new AttachmentData();
        ad.setEmailId(r.getInt("EMAILID"));
        ad.setId(r.getInt("FILEID"));
        ad.setFileName(r.getString("FILENAME"));
        if (r.getString("TYPE").equalsIgnoreCase("embedded")) {
            ad.setIsEmbedded(true);
        }
        ad.setPicture(r.getBytes("BINARYDATA"));

        return ad;
    }

    /**
     * takes a resultset and transforms it into an attachment data object
     *
     * @param r
     * @return
     * @throws SQLException
     */
    private AttachmentData createAttachmentData(int emailId, String fileName, Boolean isEmbedded, byte[] picture) throws SQLException {
        AttachmentData ad = new AttachmentData();
        ad.setEmailId(emailId);
        ad.setFileName(fileName);

        ad.setIsEmbedded(isEmbedded);

        ad.setPicture(picture);

        return ad;
    }

    /**
     * This method deletes a single record based on the criteria of the primary
     * key field ID value.It should return either 0 meaning that there is no
     * record with that ID or 1 meaning a single record was deleted. this method
     * should be called from the main app, when a user tries to delete an email
     * so that the related attachments are also deleted
     *
     * @param addressData that is being deleted
     * @return The number of records deleted, should be 0 or 1
     * @throws SQLException
     */
    @Override
    public int delete(MailConfig mc, AttachmentData ad) throws SQLException {

        int result;

        String deleteQuery = "DELETE FROM BINARYFILES WHERE ID = ?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            //checks if a record was deleted in the bridgetable before deleting from addresses
            if (deleteBridgeTableRecord(mc, ad) > 0) {
                ps.setInt(1, ad.getId());
                result = ps.executeUpdate();
            } else {
                result = 0;
            }
        }
        LOG.info("# of records deleted : " + result);
        return result;
    }

    /**
     * method that is called when an attachment is deleted in order to remove
     * it's related bridge table record
     *
     * @param ad
     * @return
     * @throws SQLException
     */
    private int deleteBridgeTableRecord(MailConfig mc, AttachmentData ad) throws SQLException {

        int result;

        String deleteQuery = "DELETE FROM EMAILBRBINARYFILES WHERE EMAILID=? AND FILEID =?";

        // Connection is only open for the operation and then immediately closed
        try ( Connection connection = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword()); // You must use PreparedStatements to guard against SQL
                // Injection
                  PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, ad.getEmailId());
            ps.setInt(2, ad.getId());
            result = ps.executeUpdate();
        }
        LOG.info("# of records deleted : " + result);
        return result;
    }

    private boolean validateMailConfig(MailConfig mc) throws SQLException {
        boolean reachable;
        try {
            Connection conn = DriverManager.getConnection(mc.getDatabaseUrl(), mc.getDatabaseUsername(), mc.getDatabasePassword());
            reachable = conn.isValid(3);
        } catch (SQLException e) {
            return false;
        }
        return reachable;

    }

}
