package com.petko.persistence;


import com.petko.beans.EmailData;
import com.petko.beans.FolderData;
import com.petko.beans.MailConfig;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.ObservableList;

/**
 * Interface for CRUD methods
 *
 * @author Petko
 */
public interface EmailDAO {

    // Create
    public int create( MailConfig mc,EmailData emailData) throws SQLException;

    // Read
    public EmailData find( MailConfig mc,int id) throws SQLException;
    public ObservableList<EmailData> findAll( MailConfig mc) throws SQLException;
    
    public ObservableList<EmailData> findAllFromFolder( MailConfig mc, FolderData folderData) throws SQLException;

    public List<EmailData> findAllContaining( MailConfig mc,String s) throws SQLException;
    
    // Update
    public int update( MailConfig mc,EmailData emailData) throws SQLException;

    // Delete
    public int delete( MailConfig mc,int ID) throws SQLException;
}
