/**
 * Here is your documentation on Jodd
 * https://jodd.org/email/index.html
 */
package com.petko.business;

import com.petko.beans.MailConfig;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Flags;
import javax.mail.SendFailedException;


import jodd.mail.EmailFilter;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;

import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

/**
 *
 * @author Petko Petkov
 * @version 1.0
 *
 */
public class SendAndReceive {

    // Real programmers use logging
    private final static Logger LOG = LoggerFactory.getLogger(SendAndReceive.class);

    /**
     * Standard send routine using Jodd.Jodd knows about GMail so no need to
     * include port information
     *
     * @param mailConfig
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     * @param message
     * @param html
     * @param regAttachments
     * @param embeddedAttachments
     * @return
     * @throws javax.mail.SendFailedException
     * @throws java.io.FileNotFoundException
     *
     */
    public Email sendEmail(MailConfig mailConfig, List<String> to, List<String> cc, List<String> bcc, String subject, String message, String html, ArrayList<File> regAttachments, ArrayList<File> embeddedAttachments) throws SendFailedException, FileNotFoundException {
        //checks if the sender is valid throw ex
        LOG.info("Starting send method");
        if (checkEmail(mailConfig.getUserEmailAddress())) {

            LOG.info("sender email address vverified");
            //checks if at LEAST one valid recipient
            if (checkEmailList(to) || checkEmailList(cc) || checkEmailList(bcc)) {
                LOG.info("valid recipient found");
                Email email = Email.create()
                        .from(mailConfig.getUserEmailAddress());
                //add custom exceptions for each if to be handled
                if (!handleTo(to, email)) {
                    LOG.info("TO is empty");
                }
                if (!handleCc(cc, email)) {
                    LOG.info("CC is empty");
                }
                if (!handleBcc(bcc, email)) {
                    LOG.info("Bcc is empty");
                }

                if (!handleSubject(subject, email)) {
                    LOG.info("Subject is empty or problematic");
                }
                if (!handleTextMessage(message, email)) {
                    LOG.info("Message is empty or problematic");
                }
                if (!handleHtml(html, email)) {
                    LOG.info("html is empty or problematic");
                }
                if (!handleRegAttachments(regAttachments, email)) {
                    LOG.info("There is an invalid regularAttachment ");
                    throw new FileNotFoundException("a file was not found");
                }
                if (!handleEmbeddedAttachments(embeddedAttachments, email)) {
                    LOG.info("There is an invalid embeddedAttachments ");
                    throw new FileNotFoundException("a file was not found");
                }
                
                // Create am SMTP server object
                SmtpServer smtpServer = createSmtpServer(mailConfig);
                try ( // A session is the object responsible for communicating with the server
                         SendMailSession session = smtpServer.createSession()) {
                    // Like a file we open the session, send the message and close the
                    // session
                    session.open();
                    session.sendMail(email);
                    LOG.info("Email sent");
                    return email;
                }

                /// catch invalid mailconfig credentials
            } else {
                LOG.info("Unable to send email because a receiver address is invalid");
                throw new SendFailedException(" there is no valid recipient ");
            }

            // Using the fluent style of coding create a plain text message
        } else {
            LOG.info("Unable to send email because either send addresses is invalid");
            throw new SendFailedException(" the sender is invalid ");

        }
    }

    /**
     * retrieves the unseen messages from an email
     *
     * @param mailConfig
     * @return array containing received emails
     * @throws javax.mail.SendFailedException
     */
    public ReceivedEmail[] receiveEmail(MailConfig mailConfig) throws SendFailedException {

        if (checkEmail(mailConfig.getUserEmailAddress())) {
            ImapServer imapServer = createImapServer(mailConfig);
            
            try ( ReceiveMailSession session = imapServer.createSession()) {
                LOG.info("receiving emails from this email: "+ mailConfig.getUserEmailAddress());
                session.open();
                LOG.info("Message count: " + session.getMessageCount());
                ReceivedEmail[] emails = session.receiveEmailAndMarkSeen(EmailFilter.filter().flag(Flags.Flag.SEEN, false));
                
                if (emails != null) {
                    return emails;
//                  
                }
            }
            
        } else {
            LOG.error("receiving emails from this email failed: "+ mailConfig.getUserEmailAddress());
                
            LOG.error("Unable to send email because either send or recieve addresses are invalid");
            throw new SendFailedException("the Receiver emaiil is invalid");
        }
        return null;
    }

    /**
     * handles ArrayList of email addresses and adds them as "to" to an email
     * object
     *
     * @param to
     * @param mail
     * @return true if added at least one "to", false if nothing is added
     */
    private boolean handleTo(List<String> to, Email mail) {
        boolean toHandled = false;
        for (String emailAddress : to) {
            if (checkEmail(emailAddress)) {
                mail.to(emailAddress);
                toHandled = true;
            }
        }
        return toHandled;
    }

    /**
     * handles ArrayList of email addresses and adds them as "cc" to an email
     * object
     *
     * @param cc
     * @param mail
     * @return true if added at least one "cc", false if nothing is added
     */
    private boolean handleCc(List<String> cc, Email mail) {
        boolean ccHandled = false;
        for (String emailAddress : cc) {
            if (checkEmail(emailAddress)) {
                mail.cc(emailAddress);
                ccHandled = true;
            }
        }
        return ccHandled;
    }

    /**
     * handles ArrayList of email addresses and adds them as "bcc" to an email
     * object
     *
     * @param bcc
     * @param mail
     * @return true if added at least one "bcc", false if nothing is added
     */
    private boolean handleBcc(List<String> bcc, Email mail) {
        boolean bccHandled = false;
        for (String emailAddress : bcc) {
            if (checkEmail(emailAddress)) {
                mail.bcc(emailAddress);
                bccHandled = true;
            }
        }
        return bccHandled;
    }

    /**
     * Adds subject to email object only if subj is not empty
     *
     * @param subj
     * @param mail
     * @return true if a subject of more than 0 char is added
     */
    private boolean handleSubject(String subj, Email mail) {
        boolean subjHandled = true;

        if (subj.isEmpty()) {
            subjHandled = false;
        }
        try {
            mail.subject(subj);
        } catch (Exception e) {
            subjHandled = false;
        }
        return subjHandled;
    }

    /**
     * adds message to an email object
     *
     * @param msg
     * @param mail
     * @return true if a text message was added to the email object, false if
     * nothing was added because msg is empty
     *
     */
    private boolean handleTextMessage(String msg, Email mail) {
        boolean msgHandled = true;
        if (msg.isEmpty()) {
            msgHandled = false;
        } else {
            mail.textMessage(msg);
        }

        return msgHandled;
    }

    /**
     * add html message to an email object
     *
     * @param html
     * @param mail
     * @return true if added, false if the stringparameter is empty
     */
    private boolean handleHtml(String html, Email mail) {
        boolean htmlHandled = true;
        if (html.isBlank()) {
            return false;
        }
        else{
            
            htmlHandled = false;
        }
        return htmlHandled;
    }

    /**
     * checks if array of files is empty, if not checks if file exists before
     * adding in as a regular attachment to an email object
     *
     * @param files
     * @param mail
     * @return true if successfully added files or if no files to add, false if
     * a given file of the array doesn't exist
     * @throws FileNotFoundException if a file is not found
     */
    private boolean handleRegAttachments(ArrayList<File> files, Email mail) throws FileNotFoundException {
        boolean handled = false;
        if (files==null ||files.isEmpty()) {
            return true;
        }
        try {
            for (File file : files) {
                if (file.exists()) {

                    mail.attachment(EmailAttachment.with().content(file.getPath()));
                    handled = true;
                } else {
                    handled = false;
                    throw new FileNotFoundException(file.getAbsolutePath() + "cannot be found");
                }
            }
        } catch (FileNotFoundException ex) {
            LOG.info(ex + "file not found");

        }

        return handled;

    }

    /**
     * checks if array of files is empty, if not checks if file exists before
     * adding in as an embedded attachment to an email object
     *
     * @param files
     * @param mail
     * @return true if successfully added files or if no files to add, false if
     * a given file of the array doesn't exist
     * @throws FileNotFoundException if a file is not found
     */
    private boolean handleEmbeddedAttachments(ArrayList<File> files, Email mail) throws FileNotFoundException {
        boolean handled = false;
        if (files==null ||files.isEmpty()) {
            return true;
        }
        try {
            for (File file : files) {
                if (file.exists()) {

                    mail.embeddedAttachment(EmailAttachment.with().content(file));
                    handled = true;
                } else {
                    handled = false;
                    throw new FileNotFoundException(file.getAbsolutePath() + "cannot be found");
                }
            }
        } catch (FileNotFoundException ex) {
            LOG.info(ex + "file not found");

        }

        return handled;

    }

    /**
     * retrieves the unseen messages from an email
     *
     * @param mailConfig
     * @return array containing received emails
     *
     * /**
     * creates an imapserver object from a mailconfig
     * @param mailConfig
     * @return
     */
    private ImapServer createImapServer(MailConfig mailConfig) {
        ImapServer imapServer = MailServer.create()
                .host(mailConfig.getImapUrl())
                .ssl(true)
                .auth(mailConfig.getUserEmailAddress(), mailConfig.getPassword())
                //.debugMode(true)
                .buildImapMailServer();

        return imapServer;
    }



    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }

    /**
     * Uses the checkEmail method to check if an ArrayList contains at least one
     * valid email address valid address
     *
     * @param emails
     * @return true if at least one valid email
     */
    private boolean checkEmailList(List<String> emails) {

        return emails.stream().anyMatch(email -> (checkEmail(email)));
    }

    /**
     * creates an SmtpServer from a mail config object
     *
     * @param mailConfig
     * @return an smtp server
     */
    private SmtpServer createSmtpServer(MailConfig mailConfig) {
        
            SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(mailConfig.getSmtpUrl())
                .auth(mailConfig.getUserEmailAddress(), mailConfig.getPassword())
                //.debugMode(true)
                .buildSmtpMailServer();
       
        
        return smtpServer;
    }
    

  
}
