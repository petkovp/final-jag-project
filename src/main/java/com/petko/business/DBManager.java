/*
 * this class is used to 
 */
package com.petko.business;

import com.petko.beans.EmailData;
import com.petko.beans.MailConfig;
import com.petko.beans.PropertyBean;
import com.petko.persistence.AddressesDAO;
import com.petko.persistence.AddressesDAOImpl;
import com.petko.persistence.AttachmentsDAO;
import com.petko.persistence.AttachmentsDAOImpl;
import com.petko.persistence.EmailDAO;
import com.petko.persistence.EmailDAOImpl;
import com.petko.persistence.FolderDAO;
import com.petko.persistence.FolderDAOImpl;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.SendFailedException;
import jodd.mail.Email;
import jodd.mail.EmailMessage;
import jodd.mail.ReceivedEmail;
import org.slf4j.LoggerFactory;

/**
 *DBManager is called by controllers to update the newly received emails and to store them to the database.
 * it is also used to save sent messages to the database
 * @author HP
 */
public class DBManager {
    
    private SendAndReceive sendAndReceive;
    private MailConfig mailConfig;
    private  EmailDAO emailDAO;
    private FolderDAO folderDAO;
    private AttachmentsDAO attachmentsDAO;
    private AddressesDAO addressesDAO;
    private final org.slf4j.Logger LOG = LoggerFactory.getLogger(getClass().getName());
    
    public DBManager(PropertyBean propertyBean){
        //this.mailConfig=propertyBean.getMailConfig();
        this.sendAndReceive = new SendAndReceive();
        this.emailDAO = new EmailDAOImpl();
        this.folderDAO = new FolderDAOImpl();
        this.addressesDAO = new AddressesDAOImpl();
        this.attachmentsDAO = new AttachmentsDAOImpl();
    }
    
    public boolean saveEmailTODB(EmailData emailData){
        
        try {
            this.emailDAO.create(mailConfig, emailData);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    /**
     * sets mailconfigbean property
     * @param propertyBean 
     */
    public void setMailConfigBean(PropertyBean propertyBean) {
        this.mailConfig = propertyBean.getMailConfig();
    }
    /**
     * method called to receive the emails and then call another method to add them to the database
     * @return 
     */
    public boolean handleReceivedEmails(){
        try {
        this.mailConfig = new MailConfig("", "smtp.gmail.com", "smtp.gmail.com", "993", "465", "jdbc:mysql://localhost:3306/MAILAPP?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true", "MAILAPP", "3306", "mailappuser", "petkostandard", "receiver.email12@gmail.com", "Dawson123@");
        
        ReceivedEmail[] receivedEmails = this.sendAndReceive.receiveEmail(mailConfig);
        
        LOG.debug("number of received emails"+ receivedEmails.length);
        receivedEmailsToDB(receivedEmails);
        } catch (SQLException ex ) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SendFailedException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }
    
    /**
     * adds the received emails to the database
     * @param receivedEmails
     * @return
     * @throws SQLException 
     */
    private boolean receivedEmailsToDB(ReceivedEmail[] receivedEmails) throws SQLException{
       
        List<EmailData> emailDatas = new ArrayList<EmailData>(receivedEmails.length);
        int addedEmails = 0;
        for(ReceivedEmail email: receivedEmails){
            
            EmailData data = new EmailData();
            data.setFolderKey(1);
            //data.setHtmlMessage(email.messages().get(0).toString());
            data.setReceivedDate(new Timestamp(email.receivedDate().getTime()));
            Email emailObj = new Email();
            emailObj.cc(email.cc());
            emailObj.to(email.to());
            emailObj.attachments(emailObj.attachments());
            emailObj.from(email.from());
            emailObj.message(email.messages());
            
             emailObj.sentDate(email.sentDate());
            
            emailObj.subject(email.subject());
            data.setEmail(emailObj);
            for(EmailMessage msg:email.messages() ){
                if(msg !=null){
                LOG.debug("message:"+msg.getContent());
                data.setHtmlMessage(msg.getContent());
                data.setRegularMessage(msg.getContent());
                }
            }
            this.emailDAO.create(mailConfig, data);
            addedEmails++;
        }
        if(addedEmails == receivedEmails.length){
            return true;
        }
        else{
            return false;
        }
    }
}
